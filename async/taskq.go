package async

import (
	"fmt"
	"gitee.com/ayun2001/ginx"
	"gitee.com/ayun2001/ginx/utils"
	"github.com/prometheus/client_golang/prometheus"
	"math"
	"sync"
)

type TaskQ struct {
	c           chan interface{}
	wg          sync.WaitGroup
	closeOnce   sync.Once
	count       uint16
	name        string
	cacheNumber prometheus.Gauge
}

func NewAsyncTaskQ(name string, count uint16, handler func(*TaskQ, interface{}) error) *TaskQ {
	if len(name) <= 0 {
		name = "queue_" + utils.Int64ToStr(utils.GetRandIdNumber())
	}

	obj := &TaskQ{
		count: count,
		name:  name,
		c:     make(chan interface{}, math.MaxUint16*10),
		cacheNumber: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace:   utils.ConstPrometheusNamespace,
			Subsystem:   utils.ConstPrometheusSubsystemName,
			Name:        "task_queue_cached_object_number",
			Help:        "The total number of cached objects",
			ConstLabels: map[string]string{utils.ConstPromAppName: utils.PromAppName, "name": name, "id": utils.Int64ToStr(utils.GetRandIdNumber())},
		}),
	}

	prometheus.MustRegister(obj.cacheNumber) // 注册 metrics

	// 启动工作线程
	for i := uint16(0); i < count; i++ {
		go obj.worker(i, handler)
	}
	obj.wg.Add(int(count))

	ginx.Logs.Infow(obj.name + " asynchronous queue start")
	// 返回结果
	return obj
}

// Close 安全关闭消息队列
func (q *TaskQ) Close() {
	q.closeOnce.Do(func() {
		close(q.c)
		q.wg.Wait()
		prometheus.Unregister(q.cacheNumber) // 卸载
		ginx.Logs.Infow(q.name + " asynchronous queue stop")
	})
}

// Publish 推送消息到队列
func (q *TaskQ) Publish(cnt interface{}) {
	q.c <- cnt
	q.cacheNumber.Inc()
}

// 消息工作队列
func (q *TaskQ) worker(id uint16, handler func(*TaskQ, interface{}) error) {
	ginx.Logs.Infow(fmt.Sprintf("%s event push daemon worker: %d start", q.name, id))

	defer q.wg.Done()

	var m interface{}
	// 读取消息体
	for m = range q.c {
		q.cacheNumber.Dec()
		// 交给 handler 处理
		if err := handler(q, m); err != nil {
			ginx.Logs.Infow(fmt.Sprintf("%s item processing failure with worker: %d", q.name, id), "error", err)
			break
		}
	}
}
