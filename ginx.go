package ginx

import "gitee.com/ayun2001/ginx/log"

var (
	ZapLogs = log.NewZapLogger()
	Logs    = log.ZapLoggerToSugaredLogger(ZapLogs)
)
