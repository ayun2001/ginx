package httpsvr

import (
	"context"
	"gitee.com/ayun2001/ginx/utils"
	"github.com/prometheus/client_golang/prometheus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"strings"
	"time"
)

const (
	unary        = "unary"
	clientStream = "client_stream"
	serverStream = "server_stream"
	bidiStream   = "bidi_stream"
)

type grpcSvrMetrics struct {
	name              string
	startedCount      *prometheus.CounterVec
	handledCount      *prometheus.CounterVec
	streamMsgReceived *prometheus.CounterVec
	streamMsgSent     *prometheus.CounterVec
	handledDuration   *prometheus.HistogramVec
}

func newGrpcSvrMetrics(name string) *grpcSvrMetrics {
	return &grpcSvrMetrics{name: name}
}

func (m *grpcSvrMetrics) registerMetrics() {
	var metricLabels = []string{"type", "service", "method", utils.ConstPromAppName, promServiceEndpoint}
	var id = utils.Int64ToStr(utils.GetRandIdNumber())

	m.startedCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstPrometheusNamespace,
			Subsystem:   utils.ConstPrometheusSubsystemName,
			Name:        "grpc_server_started_total",
			Help:        "Total number of RPCs started on the server.",
			ConstLabels: map[string]string{"name": m.name, "id": id},
		}, metricLabels)

	m.handledCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstPrometheusNamespace,
			Subsystem:   utils.ConstPrometheusSubsystemName,
			Name:        "grpc_server_handled_total",
			Help:        "Total number of RPCs completed on the server, regardless of success or failure.",
			ConstLabels: map[string]string{"name": m.name, "id": id},
		}, append(metricLabels, "code"))

	m.streamMsgReceived = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstPrometheusNamespace,
			Subsystem:   utils.ConstPrometheusSubsystemName,
			Name:        "grpc_server_msg_received_total",
			Help:        "Total number of RPC stream messages received on the server.",
			ConstLabels: map[string]string{"name": m.name, "id": id},
		}, metricLabels)

	m.streamMsgSent = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstPrometheusNamespace,
			Subsystem:   utils.ConstPrometheusSubsystemName,
			Name:        "grpc_server_msg_sent_total",
			Help:        "Total number of gRPC stream messages sent by the server.",
			ConstLabels: map[string]string{"name": m.name, "id": id},
		}, metricLabels)

	m.handledDuration = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace:   utils.ConstPrometheusNamespace,
		Subsystem:   utils.ConstPrometheusSubsystemName,
		Name:        "grpc_server_handling_seconds",
		Help:        "Histogram of response latency (seconds) of gRPC that had been application-level handled by the server.",
		ConstLabels: map[string]string{"name": m.name, "id": id},
	}, metricLabels)

	prometheus.MustRegister(m.startedCount, m.handledCount, m.streamMsgReceived, m.streamMsgSent, m.handledDuration)
}

func (m *grpcSvrMetrics) unRegisterMetrics() {
	prometheus.Unregister(m.startedCount)
	prometheus.Unregister(m.handledCount)
	prometheus.Unregister(m.streamMsgReceived)
	prometheus.Unregister(m.streamMsgSent)
	prometheus.Unregister(m.handledDuration)
}

// unaryServerInterceptor is a gRPC server-side interceptor that provides Prometheus monitoring for Unary RPCs.
func (m *grpcSvrMetrics) unaryServerInterceptor(extOpts *map[string]interface{}) func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		monitor := newServerReporter(m, unary, info.FullMethod, (*extOpts)[promServiceEndpoint].(string))
		monitor.ReceivedMessage()
		resp, err := handler(ctx, req)
		st, _ := status.FromError(err)
		monitor.Handled(st.Code())
		if err == nil {
			monitor.SentMessage()
		}
		return resp, err
	}
}

// streamServerInterceptor is a gRPC server-side interceptor that provides Prometheus monitoring for Streaming RPCs.
func (m *grpcSvrMetrics) streamServerInterceptor(extOpts *map[string]interface{}) func(srv interface{}, ss grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
	return func(srv interface{}, ss grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		monitor := newServerReporter(m, streamRpcType(info), info.FullMethod, (*extOpts)[promServiceEndpoint].(string))
		err := handler(srv, &monitoredStream{ss, monitor})
		st, _ := status.FromError(err)
		monitor.Handled(st.Code())
		return err
	}
}

func (m *grpcSvrMetrics) Describe(ch chan<- *prometheus.Desc) {
	m.startedCount.Describe(ch)
	m.handledCount.Describe(ch)
	m.streamMsgReceived.Describe(ch)
	m.streamMsgSent.Describe(ch)
	m.handledDuration.Describe(ch)
}

func (m *grpcSvrMetrics) Collect(ch chan<- prometheus.Metric) {
	m.startedCount.Collect(ch)
	m.handledCount.Collect(ch)
	m.streamMsgReceived.Collect(ch)
	m.streamMsgSent.Collect(ch)
	m.handledDuration.Collect(ch)
}

func streamRpcType(info *grpc.StreamServerInfo) string {
	if info.IsClientStream && !info.IsServerStream {
		return clientStream
	} else if !info.IsClientStream && info.IsServerStream {
		return serverStream
	}
	return bidiStream
}

type reporter struct {
	metrics     *grpcSvrMetrics
	rpcType     string
	serviceName string
	methodName  string
	startTime   time.Time
	listener    string
}

func newServerReporter(m *grpcSvrMetrics, rpcType, method, listener string) *reporter {
	r := &reporter{metrics: m, rpcType: rpcType, listener: listener}
	r.startTime = time.Now()
	r.serviceName, r.methodName = splitMethodName(method)
	r.metrics.startedCount.WithLabelValues(r.rpcType, r.serviceName, r.methodName, utils.PromAppName, r.listener).Inc()
	return r
}

func (r *reporter) ReceivedMessage() {
	r.metrics.streamMsgReceived.WithLabelValues(r.rpcType, r.serviceName, r.methodName, utils.PromAppName, r.listener).Inc()
}

func (r *reporter) SentMessage() {
	r.metrics.streamMsgSent.WithLabelValues(r.rpcType, r.serviceName, r.methodName, utils.PromAppName, r.listener).Inc()
}

func (r *reporter) Handled(code codes.Code) {
	r.metrics.handledCount.WithLabelValues(r.rpcType, r.serviceName, r.methodName, utils.PromAppName, r.listener, code.String()).Inc()
	r.metrics.handledDuration.WithLabelValues(r.rpcType, r.serviceName, r.methodName, utils.PromAppName, r.listener).Observe(time.Since(r.startTime).Seconds())
}

func splitMethodName(fullMethodName string) (string, string) {
	fullMethodName = strings.TrimPrefix(fullMethodName, "/") // remove leading slash
	if i := strings.Index(fullMethodName, "/"); i >= 0 {
		return fullMethodName[:i], fullMethodName[i+1:]
	}
	return "unknown", "unknown"
}

// monitoredStream wraps grpc.ServerStream allowing each Sent/Recv of message to increment counters.
type monitoredStream struct {
	grpc.ServerStream
	r *reporter
}

func (s *monitoredStream) SendMsg(m interface{}) error {
	err := s.ServerStream.SendMsg(m)
	if err == nil {
		s.r.SentMessage()
	}
	return err
}

func (s *monitoredStream) RecvMsg(m interface{}) error {
	err := s.ServerStream.RecvMsg(m)
	if err == nil {
		s.r.ReceivedMessage()
	}
	return err
}
