package httpsvr

import (
	"context"
	"errors"
	"fmt"
	"gitee.com/ayun2001/ginx"
	"gitee.com/ayun2001/ginx/httptool/request"
	"gitee.com/ayun2001/ginx/utils"
	"github.com/gin-gonic/gin"
	grpcMiddleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpcZap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	grpcRecovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
	"google.golang.org/grpc/reflection"
	"math"
	"net"
	"net/http"
	"strings"
	"sync"
	"time"
)

var RouteNoRegisteredError = errors.New("router group not registered")

const serviceShutdownTimeoutSec = 10

type HttpServerConf struct {
	Address                       string `json:"address" yaml:"address"`
	Port                          uint16 `json:"port" yaml:"port"`
	HttpReadTimeout               uint32 `json:"httpReadTimeout,omitempty" yaml:"httpReadTimeout,omitempty"`
	HttpWriteTimeout              uint32 `json:"httpWriteTimeout,omitempty" yaml:"httpWriteTimeout,omitempty"`
	HttpReadHeaderTimeout         uint32 `json:"httpReadHeaderTimeout,omitempty" yaml:"httpReadHeaderTimeout,omitempty"`
	GrpcClientPingMinIntervalTime uint32 `json:"grpcClientPingMinIntervalTime,omitempty" yaml:"grpcClientPingMinIntervalTime,omitempty"`
	GrpcClientIdleTimeout         uint32 `json:"grpcClientIdleTimeout,omitempty" yaml:"grpcClientIdleTimeout,omitempty"`
	GrpcConnAgeGraceTimeout       uint32 `json:"grpcConnAgeGraceTimeout,omitempty" yaml:"grpcConnAgeGraceTimeout,omitempty"`
}

type GinEngineFeatureOpts struct {
	EnableGinRedirectTrailingSlash bool // 内部 301 路径跳转，如果当前路径的处理函数不存在，但是路径+'/'的处理函数存在，则允许进行重定向
	EnableGinRedirectFixedPath     bool // 允许修复当前请求路径，如/FOO和/..//Foo会被修复为/foo，并进行重定向，默认为 false。
	EnableHttpHealthCheck          bool // 健康检查
	EnablePrometheusMetrics        bool // 记录 Prometheus 数据
	EnablePrometheusRoute          bool // 能够访问 Prometheus 数据
	EnableSwagger                  bool // Swagger
	EnableHttpPProf                bool // running time debug
	EnableGrpc                     bool // 启动 grpc over http
}

type GinEngineNoMatchHandlers = struct {
	NoRouteHandlers  []gin.HandlerFunc
	NoMethodHandlers []gin.HandlerFunc
}

type GinEngineConf struct {
	FeatureOpts     GinEngineFeatureOpts
	NoMatchHandlers GinEngineNoMatchHandlers
}

type GinEngineMiddlewareOpts struct {
	HttpSvrMiddleware []gin.HandlerFunc
	GrpcSvrMiddleware struct {
		StreamInterceptors []grpc.StreamServerInterceptor
		UnaryInterceptors  []grpc.UnaryServerInterceptor
	}
}

func NewDefaultGinEngineConf() *GinEngineConf {
	return &GinEngineConf{
		FeatureOpts: GinEngineFeatureOpts{
			EnableGinRedirectTrailingSlash: false, // 默认关闭
			EnableGinRedirectFixedPath:     false, // 默认关闭
			EnableHttpHealthCheck:          true,
			EnablePrometheusMetrics:        true,
			EnablePrometheusRoute:          true,
			EnableSwagger:                  true,
			EnableHttpPProf:                true,
			EnableGrpc:                     true,
		},
		NoMatchHandlers: GinEngineNoMatchHandlers{
			NoRouteHandlers:  nil,
			NoMethodHandlers: nil,
		},
	}
}

func NewOnlyHttpGinEngineConf() *GinEngineConf {
	return &GinEngineConf{
		FeatureOpts: GinEngineFeatureOpts{
			EnableGinRedirectTrailingSlash: false, // 默认关闭
			EnableGinRedirectFixedPath:     false, // 默认关闭
			EnableHttpHealthCheck:          true,
			EnablePrometheusMetrics:        true,
			EnablePrometheusRoute:          true,
			EnableSwagger:                  true,
			EnableHttpPProf:                true,
			EnableGrpc:                     false,
		},
		NoMatchHandlers: GinEngineNoMatchHandlers{
			NoRouteHandlers:  nil,
			NoMethodHandlers: nil,
		},
	}
}

func NewOnlyGrpcGinEngineConf() *GinEngineConf {
	return &GinEngineConf{
		FeatureOpts: GinEngineFeatureOpts{
			EnableGinRedirectTrailingSlash: false, // 默认关闭
			EnableGinRedirectFixedPath:     false, // 默认关闭
			EnableHttpHealthCheck:          true,
			EnablePrometheusMetrics:        true,
			EnablePrometheusRoute:          true,
			EnableSwagger:                  false,
			EnableHttpPProf:                true,
			EnableGrpc:                     true,
		},
		NoMatchHandlers: GinEngineNoMatchHandlers{
			NoRouteHandlers:  nil,
			NoMethodHandlers: nil,
		},
	}
}

func NewOnlyMetricsGinEngineConf() *GinEngineConf {
	return &GinEngineConf{
		FeatureOpts: GinEngineFeatureOpts{
			EnableGinRedirectTrailingSlash: false, // 默认关闭
			EnableGinRedirectFixedPath:     false, // 默认关闭
			EnableHttpHealthCheck:          true,
			EnablePrometheusMetrics:        true,
			EnablePrometheusRoute:          true,
			EnableSwagger:                  false,
			EnableHttpPProf:                false,
		},
		NoMatchHandlers: GinEngineNoMatchHandlers{
			NoRouteHandlers:  nil,
			NoMethodHandlers: nil,
		},
	}
}

type httpServiceInterface interface {
	RoutesRegistry(ge *GinEngine)
}

type GrpcRegisteredServiceItem struct {
	ServiceDesc   *grpc.ServiceDesc
	ServiceServer interface{}
}

type routeGroups struct {
	groups map[string]*gin.RouterGroup
	lock   sync.RWMutex
}

type GinEngine struct {
	RootRouteGroup *gin.RouterGroup
	ginSvr         *gin.Engine
	routeGroups    routeGroups
	httpServices   []httpServiceInterface
	grpcServices   []GrpcRegisteredServiceItem
	wg             sync.WaitGroup
	httpSvr        *http.Server
	grpcSvr        *grpc.Server
	httpConf       *HttpServerConf
	ginConf        *GinEngineConf
	closeOnce      sync.Once
	httpMetrics    *httpSvrMetrics
	grpcMetrics    *grpcSvrMetrics
}

func (g *GinEngine) initializeHttpSvrRoutes() {
	for _, service := range g.httpServices {
		service.RoutesRegistry(g)
	}
}

func (g *GinEngine) initializeGrpcServicesRegister() {
	for _, service := range g.grpcServices {
		g.grpcSvr.RegisterService(service.ServiceDesc, service.ServiceServer)
	}
}

func (g *GinEngine) HttpMiddlewareRegistry(middleware ...gin.HandlerFunc) {
	g.ginSvr.Use(middleware...)
}

func (g *GinEngine) IsReleaseMode() bool {
	return gin.IsDebugging()
}

// GetHttpRegisteredGroup 获得已经注册的路由组
func (g *GinEngine) GetHttpRegisteredGroup(path string) (*gin.RouterGroup, error) {
	if path == ConstRootUrlPath {
		return &g.ginSvr.RouterGroup, nil
	}
	g.routeGroups.lock.RLock()
	defer g.routeGroups.lock.RUnlock()
	group, ok := g.routeGroups.groups[path]
	if !ok {
		return nil, RouteNoRegisteredError
	}
	return group, nil
}

// HttpServiceRegistry 注册服务到 GinEngine
func (g *GinEngine) HttpServiceRegistry(services ...httpServiceInterface) {
	g.routeGroups.lock.Lock()
	g.httpServices = append(g.httpServices, services...)
	g.routeGroups.lock.Unlock()
}

// GrpcServiceRegistry 注册服务到 GinEngine
func (g *GinEngine) GrpcServiceRegistry(services ...GrpcRegisteredServiceItem) {
	g.routeGroups.lock.Lock()
	g.grpcServices = append(g.grpcServices, services...)
	g.routeGroups.lock.Unlock()
}

// HttpRouteGroupRegistry 注册一个路由组
func (g *GinEngine) HttpRouteGroupRegistry(path string, baseGroup *gin.RouterGroup) {
	if baseGroup == nil {
		baseGroup = &g.ginSvr.RouterGroup
	}
	g.routeGroups.lock.Lock()
	g.routeGroups.groups[path] = baseGroup.Group(path)
	g.routeGroups.lock.Unlock()
}

func (g *GinEngine) GetGrpcServer() *grpc.Server {
	return g.grpcSvr
}

func (g *GinEngine) RunAndService() {
	serverType := "http"
	// 初始化的路由
	g.initializeHttpSvrRoutes()
	// 创建 http Server 对象
	var httpHandler http.Handler
	if g.ginConf.FeatureOpts.EnableGrpc {
		serverType = "grpc/http"
		// 初始化 grpc 服务
		g.initializeGrpcServicesRegister()
		// 创建支持 grpc handler
		httpHandler = h2c.NewHandler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// 判断协议是否为http/2 && 是grpc
			if r.ProtoMajor == 2 && strings.HasPrefix(r.Header.Get("Content-Type"), "application/grpc") {
				// 按grpc方式来请求
				g.grpcSvr.ServeHTTP(w, r)
			} else {
				// 当作普通api
				g.ginSvr.ServeHTTP(w, r)
			}
		}), &http2.Server{
			MaxConcurrentStreams: math.MaxUint32,
			IdleTimeout:          0, // 使用 HttpReadTimeout 做为这里的值
		})
	} else {
		httpHandler = g.ginSvr
	}

	endpoint := fmt.Sprintf("%s:%d", g.httpConf.Address, g.httpConf.Port)
	g.httpSvr = &http.Server{
		Addr:              endpoint,
		Handler:           httpHandler,
		ReadTimeout:       time.Duration(g.httpConf.HttpReadTimeout) * time.Millisecond,
		ReadHeaderTimeout: time.Duration(g.httpConf.HttpReadHeaderTimeout) * time.Millisecond,
		WriteTimeout:      time.Duration(g.httpConf.HttpWriteTimeout) * time.Millisecond,
		IdleTimeout:       0, // 使用 HttpReadTimeout 做为这里的值
		MaxHeaderBytes:    math.MaxUint32,
	}

	// 启动服务
	g.wg.Add(1)
	go func(wg *sync.WaitGroup) {
		ginx.Logs.Infow(serverType+" server start", "addr", endpoint)
		g.httpSvr.SetKeepAlivesEnabled(true) // 默认开启 keepalive， grpc/http 共享
		if err := g.httpSvr.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			ginx.Logs.Fatalw("start service failed", "error", err)
		}
		wg.Done()
	}(&g.wg)
}

func (g *GinEngine) Close() {
	g.closeOnce.Do(func() {
		endpoint := fmt.Sprintf("%s:%d", g.httpConf.Address, g.httpConf.Port)
		// 关闭 http server
		ctx, cancel := context.WithTimeout(context.Background(), serviceShutdownTimeoutSec*time.Second)
		defer cancel()
		// 关闭 http server
		g.wg.Add(1) // 增加计数器，别忘了
		go func(wg *sync.WaitGroup) {
			if g.httpSvr != nil {
				if err := g.httpSvr.Shutdown(ctx); err != nil {
					ginx.Logs.Fatalw("http server forced to shutdown", "addr", endpoint, "error", err)
				}
			}
			if g.httpMetrics != nil {
				g.httpMetrics.unRegisterMetrics()
			}
			wg.Done()
			ginx.Logs.Infow("http server shutdown", "addr", endpoint)
		}(&g.wg)
		// 关闭 grpc server
		if g.ginConf.FeatureOpts.EnableGrpc {
			g.wg.Add(1) // 增加计数器，别忘了
			go func(wg *sync.WaitGroup) {
				if g.grpcSvr != nil {
					g.grpcSvr.Stop() // 暴力停机
				}
				if g.grpcMetrics != nil {
					g.grpcMetrics.unRegisterMetrics()
				}
				wg.Done()
				ginx.Logs.Infow("grpc server shutdown", "addr", endpoint)
			}(&g.wg)
		}
		// 等待结束
		g.wg.Wait()
	})
}

// NewGinEngine
// keepalive.EnforcementPolicy：
// MinTime：如果客户端两次 ping 的间隔小于此值，则关闭连接
// PermitWithoutStream： 即使没有 active stream, 也允许 ping
//
// keepalive.ServerParameters：
// MaxConnectionIdle：如果一个 client 空闲超过该值, 发送一个 GOAWAY, 为了防止同一时间发送大量 GOAWAY, 会在此时间间隔上下浮动 10%, 例如设置为15s，即 15+1.5 或者 15-1.5
// MaxConnectionAge：如果任意连接存活时间超过该值, 发送一个 GOAWAY
// MaxConnectionAgeGrace：在强制关闭连接之间, 允许有该值的时间完成 pending 的 rpc 请求
// Time： 如果一个 client 空闲超过该值, 则发送一个 ping 请求
// Timeout： 如果 ping 请求该时间段内未收到回复, 则认为该连接已断开
func NewGinEngine(name string, engineConf *GinEngineConf, httpConf *HttpServerConf, middlewareOpts *GinEngineMiddlewareOpts) *GinEngine {
	if len(name) <= 0 {
		name = "ginx_" + utils.Int64ToStr(utils.GetRandIdNumber())
	}
	// 创建引擎
	g := new(GinEngine)
	// 创建 metrics
	g.httpMetrics = newHttpSvrMetrics(name)
	g.httpMetrics.registerMetrics()
	g.grpcMetrics = newGrpcSvrMetrics(name)
	g.grpcMetrics.registerMetrics()
	// 初始化自定义路由组
	g.routeGroups = routeGroups{groups: map[string]*gin.RouterGroup{}}
	// middleware middlewareOpts
	if middlewareOpts == nil {
		middlewareOpts = &GinEngineMiddlewareOpts{}
	}
	// 初始 gin engine 相关配置
	g.ginConf = validGinEngineConfig(engineConf)
	// 初始 http 相关配置
	g.httpConf = validHttpServerConfig(httpConf)
	endpoint := fmt.Sprintf("%s:%d", g.httpConf.Address, g.httpConf.Port)
	epMetricsOpt := map[string]interface{}{promServiceEndpoint: endpoint} // metrics 扩展参数
	// 创建 grpc 引擎 --> over HTTP
	if g.ginConf.FeatureOpts.EnableGrpc {
		streamInterceptors := []grpc.StreamServerInterceptor{
			g.grpcMetrics.streamServerInterceptor(&epMetricsOpt),
			grpcZap.StreamServerInterceptor(ginx.ZapLogs),
			grpcRecovery.StreamServerInterceptor(),
		}
		unaryInterceptors := []grpc.UnaryServerInterceptor{
			g.grpcMetrics.unaryServerInterceptor(&epMetricsOpt),
			grpcZap.UnaryServerInterceptor(ginx.ZapLogs),
			grpcRecovery.UnaryServerInterceptor(),
		}
		g.grpcSvr = grpc.NewServer(
			grpc.StreamInterceptor(grpcMiddleware.ChainStreamServer(append(streamInterceptors, middlewareOpts.GrpcSvrMiddleware.StreamInterceptors...)...)),
			grpc.UnaryInterceptor(grpcMiddleware.ChainUnaryServer(append(unaryInterceptors, middlewareOpts.GrpcSvrMiddleware.UnaryInterceptors...)...)),
			grpc.KeepaliveEnforcementPolicy(keepalive.EnforcementPolicy{
				MinTime:             time.Duration(g.httpConf.GrpcClientPingMinIntervalTime) * time.Millisecond, // 防止 ping 攻击
				PermitWithoutStream: true,
			}),
			grpc.KeepaliveParams(keepalive.ServerParameters{
				MaxConnectionIdle:     time.Duration(g.httpConf.GrpcClientIdleTimeout) * time.Millisecond,
				MaxConnectionAgeGrace: time.Duration(g.httpConf.GrpcConnAgeGraceTimeout) * time.Millisecond,
				Time:                  time.Duration(g.httpConf.GrpcClientIdleTimeout) * time.Millisecond,
				Timeout:               5 * time.Second,
			}),
		)
		reflection.Register(g.grpcSvr)
	}
	// 创建 gin 引擎
	gin.DisableConsoleColor() // 关闭终端彩色输出
	g.ginSvr = gin.New()
	g.ginSvr.RedirectTrailingSlash = g.ginConf.FeatureOpts.EnableGinRedirectTrailingSlash // 传入是否需要路径跳转
	g.ginSvr.RedirectFixedPath = g.ginConf.FeatureOpts.EnableGinRedirectFixedPath
	g.ginSvr.Use(ginLoggerRecovery(), ginLogger(), ginRequestID(), ginCors()) // 注册给引擎，要全局
	g.ginSvr.Use(middlewareOpts.HttpSvrMiddleware...)
	g.RootRouteGroup, _ = g.GetHttpRegisteredGroup(ConstRootUrlPath)
	// 自定义 gin 引擎内部错误
	if g.ginConf.NoMatchHandlers.NoMethodHandlers == nil || len(g.ginConf.NoMatchHandlers.NoMethodHandlers) == 0 {
		g.ginSvr.NoMethod(func(c *gin.Context) {
			c.JSON(http.StatusMethodNotAllowed, request.BaseHttpResponse{
				Code:         http.StatusMethodNotAllowed,
				ErrorMessage: http.StatusText(http.StatusMethodNotAllowed),
				ErrorDetail:  strings.Join([]string{"http request method not allowed, method: ", c.Request.Method, ", path: ", c.Request.URL.Path}, ""),
			})
		})
	} else {
		g.ginSvr.NoMethod(g.ginConf.NoMatchHandlers.NoMethodHandlers...)
	}
	if g.ginConf.NoMatchHandlers.NoRouteHandlers == nil || len(g.ginConf.NoMatchHandlers.NoRouteHandlers) == 0 {
		g.ginSvr.NoRoute(func(c *gin.Context) {
			c.JSON(http.StatusNotFound, request.BaseHttpResponse{
				Code:         http.StatusNotFound,
				ErrorMessage: http.StatusText(http.StatusNotFound),
				ErrorDetail:  strings.Join([]string{"http request route mismatch, method: ", c.Request.Method, ", path: ", c.Request.URL.Path}, ""),
			})
		})
	} else {
		g.ginSvr.NoRoute(g.ginConf.NoMatchHandlers.NoRouteHandlers...)
	}
	// 添加 health_check
	if g.ginConf.FeatureOpts.EnableHttpHealthCheck {
		g.RootRouteGroup.GET(ConstHttpHealthCheckUrlPath, func(c *gin.Context) {
			c.JSON(http.StatusOK, request.BaseHttpResponse{Code: 0, ErrorMessage: "success"})
		})
	}
	// 添加 swagger
	if g.ginConf.FeatureOpts.EnableSwagger {
		// http swagger
		g.RootRouteGroup.GET(ConstHttpSwaggerUrlPath, ginSwagger.WrapHandler(swaggerFiles.Handler))
		// grpc docs ??
		// todo...
	}
	// 添加性能监控接口
	if g.ginConf.FeatureOpts.EnableHttpPProf {
		httpProfService := NewGinHttpProfService()
		g.HttpServiceRegistry(httpProfService)
	}
	// 添加 prometheus 监控接口
	if g.ginConf.FeatureOpts.EnablePrometheusMetrics {
		g.ginSvr.Use(g.httpMetrics.PromMiddleware(&epMetricsOpt))
		if g.ginConf.FeatureOpts.EnablePrometheusRoute {
			g.RootRouteGroup.GET(ConstPromMetricUrlPath, httpPromHandler(promhttp.Handler()))
		}
	}
	// 返回对象
	return g
}

func validHttpServerConfig(conf *HttpServerConf) *HttpServerConf {
	if conf != nil {
		if ip := net.ParseIP(conf.Address); ip == nil {
			conf.Address = "127.0.0.1"
		}
		if conf.Port == 0 {
			conf.Port = 8080
		}
		if conf.HttpReadTimeout < utils.ConstHttpRequestIdleConnTimeout {
			conf.HttpReadTimeout = utils.ConstHttpRequestIdleConnTimeout
		}
		if conf.HttpReadHeaderTimeout < utils.ConstHttpRequestIdleConnTimeout {
			conf.HttpReadHeaderTimeout = utils.ConstHttpRequestIdleConnTimeout
		}
		if conf.HttpWriteTimeout < utils.ConstHttpRequestIdleConnTimeout {
			conf.HttpWriteTimeout = utils.ConstHttpRequestIdleConnTimeout
		}
		if conf.GrpcClientIdleTimeout < utils.ConstHttpRequestIdleConnTimeout {
			conf.GrpcClientIdleTimeout = utils.ConstHttpRequestIdleConnTimeout
		}
		if conf.GrpcClientPingMinIntervalTime < utils.ConstGrpcClientPingMinIntervalTime {
			conf.GrpcClientPingMinIntervalTime = utils.ConstGrpcClientPingMinIntervalTime
		}
		if conf.GrpcConnAgeGraceTimeout < utils.ConstHttpRequestIdleConnTimeout {
			conf.GrpcConnAgeGraceTimeout = utils.ConstHttpRequestIdleConnTimeout
		}
		return conf
	} else {
		return &HttpServerConf{
			Address: "127.0.0.1", Port: 8080,
			HttpReadTimeout:               utils.ConstHttpRequestIdleConnTimeout,
			HttpReadHeaderTimeout:         utils.ConstHttpRequestIdleConnTimeout,
			HttpWriteTimeout:              utils.ConstHttpRequestIdleConnTimeout,
			GrpcClientIdleTimeout:         utils.ConstHttpRequestIdleConnTimeout,
			GrpcClientPingMinIntervalTime: utils.ConstGrpcClientPingMinIntervalTime,
			GrpcConnAgeGraceTimeout:       utils.ConstHttpRequestIdleConnTimeout,
		}
	}
}

func validGinEngineConfig(conf *GinEngineConf) *GinEngineConf {
	if conf != nil {
		return conf
	} else {
		return NewDefaultGinEngineConf()
	}
}
