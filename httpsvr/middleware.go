package httpsvr

import (
	"gitee.com/ayun2001/ginx"
	"gitee.com/ayun2001/ginx/httptool/request"
	"gitee.com/ayun2001/ginx/utils"
	"github.com/bwmarrin/snowflake"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"net"
	"net/http"
	"os"
	"runtime/debug"
	"strings"
	"time"
)

// 解决跨站
func ginCors() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Headers", "Content-Type, AccessToken, X-CSRF-Token, Authorization, Token")
		c.Header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
		c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type")
		c.Header("Access-Control-Allow-Credentials", "true")
		if method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
		}
		c.Next()
	}
}

func ginLogger() gin.HandlerFunc {
	return func(c *gin.Context) {
		reqUrlPath := c.Request.URL.Path
		// 判断确认需要做日子记录的路劲
		if reqUrlPath == ConstHttpHealthCheckUrlPath || reqUrlPath == ConstPromMetricUrlPath {
			c.Next()
			return
		}

		// 正常处理系统日志
		start := time.Now()
		path := request.GenerateRequestPath(c)
		body := request.GenerateRequestBody(c)
		contentType := c.ContentType()

		c.Next()
		end := time.Now()
		latency := end.Sub(start)

		if len(c.Errors) > 0 {
			for _, e := range c.Errors.Errors() {
				ginx.Logs.Desugar().Error(e)
			}
		} else {
			ginx.Logs.Desugar().With(
				zap.String("requestID", c.GetHeader(request.XRequestIDHeaderKey)),
				zap.Int("status", c.Writer.Status()),
				zap.String("method", c.Request.Method),
				zap.String("contentType", contentType),
				zap.String("clientIP", c.ClientIP()),
				zap.String("clientEndpoint", c.Request.RemoteAddr),
				zap.String("path", path),
				zap.String("latency", latency.String()),
				zap.String("userAgent", c.Request.UserAgent()),
				zap.String("requestQuery", c.Request.URL.RawQuery),
				zap.String("requestBody", body)).Info("http access log")
		}
	}
}

// GinRecovery recover掉项目可能出现的panic，并使用zap记录相关日志
func ginLoggerRecovery() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			var err interface{} // 兼容 golang 1.16，1.17，1.18
			if err = recover(); err != nil {
				// Check for a broken connection, as it is not really a
				// condition that warrants a panic f trace.
				var brokenPipe bool
				if ne, ok := err.(*net.OpError); ok {
					if se, ok := ne.Err.(*os.SyscallError); ok {
						if strings.Contains(strings.ToLower(se.Error()), "broken pipe") || strings.Contains(strings.ToLower(se.Error()), "connection reset by peer") {
							brokenPipe = true
						}
					}
				}

				if brokenPipe {
					ginx.Logs.Desugar().Error("broken connection", zap.Any("error", err))
				} else {
					path := request.GenerateRequestPath(c)
					body := request.GenerateRequestBody(c)
					contentType := c.ContentType()
					ginx.Logs.Desugar().With(
						zap.String("requestID", c.GetHeader(request.XRequestIDHeaderKey)),
						zap.Any("error", err),
						zap.Int("status", c.Writer.Status()),
						zap.String("method", c.Request.Method),
						zap.String("contentType", contentType),
						zap.String("clientIP", c.ClientIP()),
						zap.String("clientEndpoint", c.Request.RemoteAddr),
						zap.String("path", path),
						zap.String("userAgent", c.Request.UserAgent()),
						zap.String("requestQuery", c.Request.URL.RawQuery),
						zap.String("requestBody", body),
						zap.String("stack", string(debug.Stack()))).Error("recovery from panic")
				}

				// If the connection is dead, we can't write a status to it.
				if brokenPipe {
					_ = c.Error(err.(error)) // nolint: errcheck
					c.Abort()
				} else {
					//c.AbortWithStatus(http.StatusInternalServerError)
					// 输出到内容到 http
					c.JSON(http.StatusInternalServerError, request.BaseHttpResponse{
						Code:         http.StatusInternalServerError,
						ErrorMessage: http.StatusText(http.StatusInternalServerError),
						ErrorDetail:  strings.Join([]string{"http server internal error, method: ", c.Request.Method, ", path: ", c.Request.URL.Path}, ""),
					})
					c.Abort()
				}

			}
		}()

		// 下一个请求
		c.Next()
	}
}

func ginRequestID() gin.HandlerFunc {
	snowflakeNode, _ := snowflake.NewNode(utils.GetRand().Int63n(-1 ^ (-1 << snowflake.NodeBits)))
	return func(c *gin.Context) {
		rid := c.GetHeader(request.XRequestIDHeaderKey)
		if len(rid) <= 0 {
			rid = utils.Int64ToStr(snowflakeNode.Generate().Int64())
			c.Request.Header.Add(request.XRequestIDHeaderKey, rid)
		}
		c.Header(request.XRequestIDHeaderKey, rid)
		c.Next()
	}
}
