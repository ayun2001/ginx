package httpsvr

import (
	"gitee.com/ayun2001/ginx"
	"github.com/gin-gonic/gin"
	"net/http"
	"net/http/pprof"
)

type GinHttpProfService struct {
}

func pprofHandler(h http.HandlerFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}

func NewGinHttpProfService() *GinHttpProfService {
	return &GinHttpProfService{}
}

func (gp GinHttpProfService) RoutesRegistry(ge *GinEngine) {
	routeRegisteredPath := ConstRootUrlPath
	g, err := ge.GetHttpRegisteredGroup(routeRegisteredPath)
	if err != nil {
		ginx.Logs.Fatalw("none group path", "path", routeRegisteredPath)
		return
	}

	rg := g.Group("/debug/pprof")

	// Get
	rg.GET("/", pprofHandler(pprof.Index))
	rg.GET("/cmdline", pprofHandler(pprof.Cmdline))
	rg.GET("/profile", pprofHandler(pprof.Profile))
	rg.GET("/symbol", pprofHandler(pprof.Symbol))
	rg.GET("/trace", pprofHandler(pprof.Trace))
	rg.GET("/allocs", pprofHandler(pprof.Handler("allocs").ServeHTTP))
	rg.GET("/block", pprofHandler(pprof.Handler("block").ServeHTTP))
	rg.GET("/goroutine", pprofHandler(pprof.Handler("goroutine").ServeHTTP))
	rg.GET("/heap", pprofHandler(pprof.Handler("heap").ServeHTTP))
	rg.GET("/mutex", pprofHandler(pprof.Handler("mutex").ServeHTTP))
	rg.GET("/threadcreate", pprofHandler(pprof.Handler("threadcreate").ServeHTTP))

	// Post
	rg.POST("/pprof/symbol", pprofHandler(pprof.Symbol))
}
