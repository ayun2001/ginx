package httpsvr

import (
	"gitee.com/ayun2001/ginx/utils"
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"net/http"
	"strconv"
	"time"
)

const promServiceEndpoint = "listener"

type httpSvrMetrics struct {
	name              string
	requestCount      *prometheus.CounterVec
	requestDuration   *prometheus.HistogramVec
	requestSizeBytes  *prometheus.SummaryVec
	responseSizeBytes *prometheus.SummaryVec
}

func newHttpSvrMetrics(name string) *httpSvrMetrics {
	return &httpSvrMetrics{name: name}
}

func (m *httpSvrMetrics) registerMetrics() {
	var metricLabels = []string{"status", "method", "client", "path", utils.ConstPromAppName, promServiceEndpoint}
	var id = utils.Int64ToStr(utils.GetRandIdNumber())

	m.requestCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstPrometheusNamespace,
			Subsystem:   utils.ConstPrometheusSubsystemName,
			Name:        "http_server_request_count",
			Help:        "Total number of HTTP requests made.",
			ConstLabels: map[string]string{"name": m.name, "id": id},
		}, metricLabels,
	)

	m.requestDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace:   utils.ConstPrometheusNamespace,
			Subsystem:   utils.ConstPrometheusSubsystemName,
			Name:        "http_server_request_duration_seconds",
			Help:        "HTTP request latencies in seconds.",
			ConstLabels: map[string]string{"name": m.name, "id": id},
		}, metricLabels,
	)

	m.requestSizeBytes = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Namespace:   utils.ConstPrometheusNamespace,
			Subsystem:   utils.ConstPrometheusSubsystemName,
			Name:        "http_server_request_size_bytes",
			Help:        "HTTP request sizes in bytes.",
			ConstLabels: map[string]string{"name": m.name, "id": id},
		}, metricLabels,
	)

	m.responseSizeBytes = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Namespace:   utils.ConstPrometheusNamespace,
			Subsystem:   utils.ConstPrometheusSubsystemName,
			Name:        "http_server_response_size_bytes",
			Help:        "HTTP response sizes in bytes.",
			ConstLabels: map[string]string{"name": m.name, "id": id},
		}, metricLabels,
	)

	prometheus.MustRegister(m.requestCount, m.requestDuration, m.requestSizeBytes, m.responseSizeBytes)
}

func (m *httpSvrMetrics) unRegisterMetrics() {
	prometheus.Unregister(m.requestCount)
	prometheus.Unregister(m.requestDuration)
	prometheus.Unregister(m.requestSizeBytes)
	prometheus.Unregister(m.responseSizeBytes)
}

// PromMiddleware returns a gin.HandlerFunc for exporting some Web metrics
func (m *httpSvrMetrics) PromMiddleware(extOpts *map[string]interface{}) gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		c.Next()

		if skipResource(c) {
			return
		}

		status := strconv.Itoa(c.Writer.Status())
		path := c.Request.URL.Path
		method := c.Request.Method
		client := c.ClientIP()
		app := utils.PromAppName
		endpoint := (*extOpts)[promServiceEndpoint].(string)

		// no response content will return -1
		respSize := c.Writer.Size()
		if respSize < 0 {
			respSize = 0
		}

		m.requestCount.WithLabelValues(status, method, client, path, app, endpoint).Inc()
		m.requestDuration.WithLabelValues(status, method, client, path, app, endpoint).Observe(time.Since(start).Seconds())
		m.requestSizeBytes.WithLabelValues(status, method, client, path, app, endpoint).Observe(calcRequestSize(c.Request))
		m.responseSizeBytes.WithLabelValues(status, method, client, path, app, endpoint).Observe(float64(respSize))
	}
}

// calcRequestSize returns the size of request object.
func calcRequestSize(r *http.Request) float64 {
	size := 0
	if r.URL != nil {
		size = len(r.URL.String())
	}

	size += len(r.Method)
	size += len(r.Proto)

	for name, values := range r.Header {
		size += len(name)
		for _, value := range values {
			size += len(value)
		}
	}
	size += len(r.Host)

	// r.Form and r.MultipartForm are assumed to be included in r.URL.
	if r.ContentLength != -1 {
		size += int(r.ContentLength)
	}
	return float64(size)
}

func skipResource(c *gin.Context) bool {
	if c.Request.URL.Path == ConstPromMetricUrlPath || c.Request.URL.Path == ConstHttpHealthCheckUrlPath {
		return true
	}
	return false
}

// httpPromHandler wrappers the standard http.Handler to gin.HandlerFunc
func httpPromHandler(handler http.Handler) gin.HandlerFunc {
	return func(c *gin.Context) {
		handler.ServeHTTP(c.Writer, c.Request)
	}
}
