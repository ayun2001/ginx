package httpsvr

const (
	ConstPromMetricUrlPath      = "/metrics"
	ConstHttpHealthCheckUrlPath = "/healthcheck"
	ConstRootUrlPath            = "/"
	ConstHttpSwaggerUrlPath     = "/docs/*any"
)
