package httpsvr

import (
	"gitee.com/ayun2001/ginx"
	"github.com/gin-gonic/gin"
)

type BaseHttpService struct {
	routeHandler func(*gin.RouterGroup)
}

func (bhs *BaseHttpService) RoutesRegistry(ge *GinEngine) {
	g, err := ge.GetHttpRegisteredGroup(ConstRootUrlPath)
	if err != nil {
		ginx.Logs.Fatalw("no registered group path", "path", ConstRootUrlPath)
		return
	}

	bhs.routeHandler(g)
}

func NewBaseHttpService(f func(*gin.RouterGroup)) *BaseHttpService {
	if f != nil {
		return &BaseHttpService{routeHandler: f}
	} else {
		ginx.Logs.Fatalw("handler func is nil")
		return nil
	}
}
