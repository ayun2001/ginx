package mysql

const (
	ConstMysqlUtf8Charset          = "utf8"
	ConstMysqlUtf8mb4Charset       = "utf8mb4"
	ConstDbConnectTimeoutSec       = 10
	ConstHealthCheckStopTimeoutSec = 5
	ConstSlowQueryTimeThreshold    = 2000 // 毫秒
	ConstExecBatchDefaultSize      = 50   // 一次执行批量任务最大多少
	ConstMysqlDefaultDbName        = "test_db"
)

type ClientConf struct {
	Address            string `json:"address" yaml:"address"`
	Port               uint16 `json:"port" yaml:"port"`
	ReadTimeout        uint32 `json:"readTimeout,omitempty" yaml:"readTimeout,omitempty"`
	WriteTimeout       uint32 `json:"writeTimeout,omitempty" yaml:"writeTimeout,omitempty"`
	Username           string `json:"username" yaml:"username"`
	Password           string `json:"password" yaml:"password"`
	DB                 string `json:"db" yaml:"db"`
	Charset            string `json:"charset" yaml:"charset"`
	MaxIdle            int    `json:"maxIdle,omitempty" yaml:"maxIdle,omitempty"`
	MaxOpen            int    `json:"maxOpen,omitempty" yaml:"maxOpen,omitempty"`
	SlowQueryThreshold uint32 `json:"slowQueryThreshold,omitempty" yaml:"slowQueryThreshold,omitempty"`
	ExecBatchSize      uint16 `json:"execBatchSize,omitempty" yaml:"execBatchSize,omitempty"`
}
