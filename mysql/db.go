package mysql

import (
	"context"
	"database/sql"
	"fmt"
	"gitee.com/ayun2001/ginx"
	"gitee.com/ayun2001/ginx/utils"
	"go.uber.org/zap"
	"gopkg.in/fatih/set.v0"
	mysqlDrv "gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"net"
	"sync"
	"time"
)

type Client struct {
	Object     *gorm.DB
	dbConnPool *sql.DB
	closeOnce  sync.Once
	stopCtx    context.Context
	stopCancel context.CancelFunc
	wg         sync.WaitGroup // go 关闭同步组
	dbConf     *ClientConf
}

var defaultCharsets = set.New(set.ThreadSafe)

func init() {
	defaultCharsets.Add(ConstMysqlUtf8Charset)
	defaultCharsets.Add(ConstMysqlUtf8mb4Charset)
}

func closeSqlConn(c *sql.DB) {
	if err := c.Close(); err != nil {
		ginx.Logs.Errorw("failed to close the connection", "error", err)
	}
}

func NewMysqlClient(conf *ClientConf) *Client {
	// 判断地址
	if ip := net.ParseIP(conf.Address); ip == nil {
		conf.Address = "127.0.0.1"
	}
	// 判断端口
	if conf.Port == 0 {
		conf.Port = 3306
	}
	// 判断DB名称
	if conf.DB == "" {
		conf.DB = ConstMysqlDefaultDbName
	}

	// 确保字符集一定是 utf8 和 utf8mb4
	if !defaultCharsets.Has(conf.Charset) {
		conf.Charset = ConstMysqlUtf8mb4Charset
	}
	// 确保慢查询时长有效
	if conf.SlowQueryThreshold == 0 {
		conf.SlowQueryThreshold = ConstSlowQueryTimeThreshold
	}
	// 确保批量执行数据有效
	if conf.ExecBatchSize == 0 {
		conf.ExecBatchSize = ConstExecBatchDefaultSize
	}

	// 创建对象
	client := &Client{dbConf: conf}
	client.stopCtx, client.stopCancel = context.WithCancel(context.Background())

	// 返回对象
	return client
}

// Close
// 这里优化了关闭顺序，优先健康监测关闭，要不然关闭出现监测失败
// 指针表示当前对象地址，没有指针表示数据副本是一个copy
func (c *Client) Close() {
	c.closeOnce.Do(func() {
		c.stopCancel()
		// 等待关闭守护
		c.wg.Wait()
		// 关闭连接池
		if c.Object != nil && c.dbConnPool != nil {
			err := c.dbConnPool.Close()
			if err != nil {
				ginx.Logs.Warnw("error when shutdown mysql connection", "error", err)
			}
			ginx.Logs.Infow("mysql client closed")
		}
	})
}

func (c *Client) HealthCheck() error {
	return c.dbConnPool.PingContext(*utils.CreateCtxWithTimeout(ConstDbConnectTimeoutSec))
}

func (c *Client) CreateDatabase() bool {
	var (
		err       error
		dbSqlConn *sql.DB
	)

	// 创建连接实例, 只有连接 MySQL 根环境，才能自动创建数据
	dbUrl := fmt.Sprintf("%s:%s@tcp(%s:%d)/?charset=%s&parseTime=True&loc=Local",
		c.dbConf.Username, c.dbConf.Password, c.dbConf.Address, c.dbConf.Port, c.dbConf.Charset)

	// 先创建 mysql 连接，判断数据库是否存在，如果不存在然后创建数据库
	dbSqlConn, err = sql.Open("mysql", dbUrl)
	if err != nil {
		ginx.Logs.Fatalw("create mysql init connection failed", "error", err)
		return false
	}

	// 关闭初始化连接
	defer closeSqlConn(dbSqlConn)
	// 创建对应的数据库
	_, err = dbSqlConn.Exec("CREATE DATABASE IF NOT EXISTS " + c.dbConf.DB + ";")
	if err != nil {
		ginx.Logs.Fatalw(fmt.Sprintf("create database %s failed", c.dbConf.DB), "error", err)
		return false
	}

	// 返回
	ginx.Logs.Infow(fmt.Sprintf("create db: %s success if not exists", c.dbConf.DB))
	return true
}

func (c *Client) Connect() bool {
	var (
		err error
	)
	// 创建 gorm 数据库连接
	dbUrl := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s&parseTime=True&loc=Local",
		c.dbConf.Username, c.dbConf.Password, c.dbConf.Address, c.dbConf.Port, c.dbConf.DB, c.dbConf.Charset)
	logger := newGormLogger(zap.InfoLevel, zap.InfoLevel, c.dbConf.SlowQueryThreshold)

	c.Object, err = gorm.Open(mysqlDrv.Open(dbUrl), &gorm.Config{
		Logger:                 logger,                      // 设置日志为 zap 日志格式
		CreateBatchSize:        int(c.dbConf.ExecBatchSize), // 创建& 关联 INSERT 一次批量执行的数量
		SkipDefaultTransaction: false,                       // 对于写操作（创建、更新、删除），为了确保数据的完整性，GORM 会将它们封装在事务内运行
		DryRun:                 false,                       // 关闭生成 SQL 但不执行的功能
		NamingStrategy: schema.NamingStrategy{
			TablePrefix:   "t_", // 表名前缀
			SingularTable: true, // 使用单数表名，启用该选项后，`User` 表将是`user`
		},
	})
	if err != nil {
		ginx.Logs.Fatalw("failed to connect to mysql", "error", err)
		return false
	}
	// 获得 Mysql 连接池实例
	c.dbConnPool, err = c.Object.DB()
	if err != nil {
		ginx.Logs.Fatalw("failed to get one mysql connection from the pool", "error", err)
		return false
	}
	// 设置连接池参数
	if c.dbConf.MaxIdle <= 0 {
		c.dbConf.MaxIdle = 10
	}
	c.dbConnPool.SetMaxIdleConns(c.dbConf.MaxIdle)
	c.dbConnPool.SetMaxOpenConns(c.dbConf.MaxOpen)
	c.dbConnPool.SetConnMaxLifetime(2 * time.Hour) // 连接连续复用窗口时间为2小时。太长， mysql 可能有问题，太短，效率差

	// 连接测试下
	err = c.HealthCheck()
	if err != nil {
		ginx.Logs.Fatalw("failed to connect to mysql", "error", err)
		c.Close()
		return false
	}
	// 设置连接池监控守护
	go c.pingHealthCheckDaemon()
	c.wg.Add(1)

	// 返回
	ginx.Logs.Infow(fmt.Sprintf("connect mysql %s:%d#%s success", c.dbConf.Address, c.dbConf.Port, c.dbConf.DB))
	return true
}

func (c *Client) GetSession(val interface{}) *gorm.DB {
	return c.Object.Model(val)
}

func (c *Client) GetCustomSession(val interface{}, opts *gorm.Session) *gorm.DB {
	return c.Object.Model(val).Session(opts)
}

func (c *Client) GetPrepareStmtSession(val interface{}) *gorm.DB {
	return c.GetCustomSession(val, &gorm.Session{PrepareStmt: true})
}

func (c *Client) GetSkipTransactionSession(val interface{}) *gorm.DB {
	return c.GetCustomSession(val, &gorm.Session{SkipDefaultTransaction: true})
}

func (c *Client) pingHealthCheckDaemon() {
	ginx.Logs.Infow("mysql health check daemon start")
	var ticker = time.NewTicker(time.Second * ConstHealthCheckStopTimeoutSec)

	defer func() {
		ticker.Stop()
		c.wg.Done()
		ginx.Logs.Infow("mysql health check daemon shutdown")
	}()

	for {
		select {
		case <-ticker.C:
			// 执行探测
			if err := c.HealthCheck(); err != nil {
				ginx.Logs.Errorw("mysql health check error", "error", err)
			}
			break
		case <-c.stopCtx.Done():
			return
		}
	}
}
