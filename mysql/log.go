package mysql

import (
	"context"
	"gitee.com/ayun2001/ginx"
	"gitee.com/ayun2001/ginx/utils"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	gormNativeLogger "gorm.io/gorm/logger"
	"time"
)

// gormLogger 使用 zap 来打印 gorm 的日志
// 初始化时在内部的 logger 中添加 trace id 可以追踪 sql 执行记录
type gormLogger struct {
	// 日志级别
	logLevel zapcore.Level
	// 指定慢查询时间记录阀值
	slowQueryThreshold time.Duration
	// Trace 方法打印日志是使用的日志 level
	traceLogWithLevel zapcore.Level
}

var gormLogLevelMap = map[gormNativeLogger.LogLevel]zapcore.Level{
	gormNativeLogger.Info:  zap.InfoLevel,
	gormNativeLogger.Warn:  zap.WarnLevel,
	gormNativeLogger.Error: zap.ErrorLevel,
}

func (g *gormLogger) Info(ctx context.Context, msg string, data ...interface{}) {
	if g.logLevel <= zap.InfoLevel {
		ginx.Logs.Infof(msg, data...)
	}
}

func (g *gormLogger) Warn(ctx context.Context, msg string, data ...interface{}) {
	if g.logLevel <= zap.WarnLevel {
		ginx.Logs.Warnf(msg, data...)
	}
}

func (g *gormLogger) Error(ctx context.Context, msg string, data ...interface{}) {
	if g.logLevel <= zap.ErrorLevel {
		ginx.Logs.Errorf(msg, data...)
	}
}

func (g *gormLogger) Trace(ctx context.Context, begin time.Time, fc func() (string, int64), err error) {
	now := time.Now()
	latency := now.Sub(begin).Seconds()
	sql, rows := fc()
	sql = utils.RemoveDuplicateWhitespace(sql, true)
	logger := ginx.Logs
	switch {
	case err != nil:
		logger.Errorw("error when exec sql", "sql", sql, "latency(ms)", utils.TruncateDecimal4(latency*1000),
			"rows", rows, "error", err.Error())
	case g.slowQueryThreshold != 0 && latency > g.slowQueryThreshold.Seconds():
		logger.Warnw("slow query when time threshold exceeded",
			"sql", sql, "latency(ms)", utils.TruncateDecimal4(latency*1000), "rows", rows,
			"threshold(ms)", g.slowQueryThreshold.Milliseconds())
	default:
		log := logger.Debugw
		if g.traceLogWithLevel == zap.InfoLevel {
			log = logger.Infow
		} else if g.traceLogWithLevel == zap.WarnLevel {
			log = logger.Warnw
		} else if g.traceLogWithLevel == zap.ErrorLevel {
			log = logger.Errorw
		}
		log("success", "sql", sql, "latency(ms)", utils.TruncateDecimal4(latency*1000), "rows", rows)
	}
}

// LogMode 实现 gorm logger 接口方法
func (g *gormLogger) LogMode(gormLogLevel gormNativeLogger.LogLevel) gormNativeLogger.Interface {
	zapLevel, exists := gormLogLevelMap[gormLogLevel]
	if !exists {
		zapLevel = zap.DebugLevel
	}
	newLogger := *g
	newLogger.logLevel = zapLevel
	return &newLogger
}

func newGormLogger(logLevel, traceLogWithLevel zapcore.Level, slowQueryTimeThreshold uint32) *gormLogger {
	return &gormLogger{
		logLevel:           logLevel,
		slowQueryThreshold: time.Duration(slowQueryTimeThreshold) * time.Millisecond,
		traceLogWithLevel:  traceLogWithLevel,
	}
}
