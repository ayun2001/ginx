package mysql

import "time"

type DefaultTimeField struct {
	CreatedAt *time.Time `gorm:"column:created_at" json:"createdAt,omitempty" yaml:"createdAt,omitempty" swaggerignore:"true"`
	UpdatedAt *time.Time `gorm:"column:updated_at" json:"updatedAt,omitempty" yaml:"updatedAt,omitempty" swaggerignore:"true"`
}

type BaseModel struct {
	ID int64 `gorm:"primary_key;column:id" json:"id,omitempty" yaml:"id,omitempty" swaggerignore:"true"`
	DefaultTimeField
}
