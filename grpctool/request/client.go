package request

import (
	"fmt"
	"gitee.com/ayun2001/ginx"
	"gitee.com/ayun2001/ginx/utils"
	grpcMiddleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpcZap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	grpc_retry "github.com/grpc-ecosystem/go-grpc-middleware/retry"
	"google.golang.org/grpc"
	"google.golang.org/grpc/connectivity"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/keepalive"
	"math"
	"net"
	"sync"
	"time"
)

type GrpcClientMiddlewareOpts struct {
	GrpcClientMiddleware struct {
		StreamInterceptors []grpc.StreamClientInterceptor
		UnaryInterceptors  []grpc.UnaryClientInterceptor
	}
}

type GrpcClientPoolConf struct {
	ServerAddress       string `json:"serverAddress" yaml:"serverAddress"`
	ServerPort          uint16 `json:"serverPort" yaml:"serverPort"`
	DisableKeepalive    bool   `json:"disableKeepalive,omitempty" yaml:"disable_keepalive,omitempty"`
	PingIntervalTimeout uint32 `json:"pingIntervalTimeout,omitempty" yaml:"pingIntervalTimeout,omitempty"`
	RetryCount          uint32 `json:"retryCount,omitempty" yaml:"retryCount,omitempty"`
	RetryPerCallTimeout uint32 `json:"retryPerCallTimeout,omitempty" yaml:"retryPerCallTimeout,omitempty"`
}

type GrpcClientPool struct {
	dialOpts       *[]grpc.DialOption
	middlewareOpts *GrpcClientMiddlewareOpts
	clientMetrics  *grpcClientMetrics
	conf           *GrpcClientPoolConf
	pool           *Pool
	closeOnce      sync.Once
}

func NewGrpcClientPool(name string, poolConf *GrpcClientPoolConf, dialOpts *[]grpc.DialOption, middlewareOpts *GrpcClientMiddlewareOpts) (*GrpcClientPool, error) {
	if len(name) <= 0 {
		name = "clientpool_" + utils.Int64ToStr(utils.GetRandIdNumber())
	}

	// 初始化配置
	poolConf = validGrpcClientConfig(poolConf)
	// middleware middlewareOpts
	if middlewareOpts == nil {
		middlewareOpts = &GrpcClientMiddlewareOpts{}
	}

	var err error
	p := GrpcClientPool{middlewareOpts: middlewareOpts, conf: poolConf, dialOpts: dialOpts}
	p.clientMetrics = newGrpcClientMetrics(name)
	p.clientMetrics.registerMetrics()
	p.pool, err = NewClientConnPool(1, math.MaxUint16<<4, p.newGrpcClient)
	if err != nil {
		return nil, err
	}
	p.pool.Ping = nil // 跳过池的健康检测
	p.pool.Close = func(c interface{}) {
		_ = c.(*grpc.ClientConn).Close() // 关闭连接
	}

	return &p, nil
}

// keepalive.ClientParameters 参数的含义如下:
// Time：如果没有 activity， 则每隔此值发送一个 ping 包
// Timeout： 如果 ping ack 该值之内未返回则认为连接已断开
// PermitWithoutStream：如果没有 active 的 stream， 是否允许发送 ping
func (p *GrpcClientPool) newGrpcClient() (interface{}, error) {
	endpoint := fmt.Sprintf("%s:%d", p.conf.ServerAddress, p.conf.ServerPort)
	epMetricsOpt := map[string]interface{}{promHostEndpoint: endpoint} // metrics 扩展参数
	streamInterceptors := []grpc.StreamClientInterceptor{
		p.clientMetrics.streamClientInterceptor(&epMetricsOpt),
		grpcZap.StreamClientInterceptor(ginx.ZapLogs),
		grpc_retry.StreamClientInterceptor(
			grpc_retry.WithMax(uint(p.conf.RetryCount)),
			grpc_retry.WithPerRetryTimeout(time.Duration(p.conf.RetryPerCallTimeout)*time.Millisecond),
		),
	}
	unaryInterceptors := []grpc.UnaryClientInterceptor{
		p.clientMetrics.unaryClientInterceptor(&epMetricsOpt),
		grpcZap.UnaryClientInterceptor(ginx.ZapLogs),
		grpc_retry.UnaryClientInterceptor(
			grpc_retry.WithMax(uint(p.conf.RetryCount)),
			grpc_retry.WithPerRetryTimeout(time.Duration(p.conf.RetryPerCallTimeout)*time.Millisecond),
		),
	}
	opts := []grpc.DialOption{
		grpc.WithTransportCredentials(insecure.NewCredentials()), // 证书认证，这个支持 insecure 模式
		grpc.WithStreamInterceptor(grpcMiddleware.ChainStreamClient(append(streamInterceptors, p.middlewareOpts.GrpcClientMiddleware.StreamInterceptors...)...)),
		grpc.WithUnaryInterceptor(grpcMiddleware.ChainUnaryClient(append(unaryInterceptors, p.middlewareOpts.GrpcClientMiddleware.UnaryInterceptors...)...)),
	}
	// 增加额外的连接选项
	if p.dialOpts != nil {
		opts = append(opts, *p.dialOpts...)
	}
	// 判断是否 keepalive
	if !p.conf.DisableKeepalive {
		opts = append(opts, grpc.WithKeepaliveParams(
			keepalive.ClientParameters{
				Time:                time.Duration(p.conf.PingIntervalTimeout) * time.Millisecond,
				Timeout:             5 * time.Second,
				PermitWithoutStream: true,
			},
		))
	}
	// 连接服务端
	conn, err := grpc.Dial(endpoint, opts...)
	if err != nil {
		return nil, err
	}
	return conn, nil
}

func (p *GrpcClientPool) GetConnect() (*grpc.ClientConn, error) {
	c, err := p.pool.Get()
	if err != nil {
		return nil, err
	}
	p.clientMetrics.clientNumber.Inc()
	return c.(*grpc.ClientConn), nil
}

// PutConnect 正常回归，返回 connection 是没有错误的
func (p *GrpcClientPool) PutConnect(c *grpc.ClientConn) {
	var s connectivity.State
	if c != nil {
		s = c.GetState()
		switch s {
		case connectivity.Idle, connectivity.Connecting, connectivity.Ready, connectivity.TransientFailure:
			p.pool.Put(c)
			break
		default:
			_ = c.Close()
		}
		p.clientMetrics.clientNumber.Dec()
	}
}

func (p *GrpcClientPool) Close() {
	p.closeOnce.Do(func() {
		p.pool.Destroy()
		p.clientMetrics.unRegisterMetrics()
	})
}

func validGrpcClientConfig(conf *GrpcClientPoolConf) *GrpcClientPoolConf {
	if conf != nil {
		if ip := net.ParseIP(conf.ServerAddress); ip == nil {
			conf.ServerAddress = "127.0.0.1"
		}
		if conf.ServerPort == 0 {
			conf.ServerPort = 8080
		}
		if conf.PingIntervalTimeout < utils.ConstHttpRequestIdleConnTimeout {
			conf.PingIntervalTimeout = utils.ConstHttpRequestIdleConnTimeout
		}
		if conf.RetryCount < 5 {
			conf.RetryCount = 5
		}
		if conf.RetryPerCallTimeout < utils.ConstGrpcClientPingMinIntervalTime {
			conf.RetryPerCallTimeout = utils.ConstGrpcClientPingMinIntervalTime
		}
		return conf
	} else {
		return &GrpcClientPoolConf{
			ServerAddress:       "127.0.0.1",
			ServerPort:          8080,
			DisableKeepalive:    false,
			PingIntervalTimeout: utils.ConstHttpRequestIdleConnTimeout,
			RetryCount:          5,
			RetryPerCallTimeout: utils.ConstGrpcClientPingMinIntervalTime,
		}
	}
}
