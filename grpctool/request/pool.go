package request

import (
	"errors"
	"sync"
)

var (
	ConnectionPoolClosedError          = errors.New("connection pool has been closed")
	ConnectionPoolInvalidCapacityError = errors.New("connection pool invalid capacity settings")
	ConnectionPoolEmptyObjectError     = errors.New("Pool.NewClientConnPool is nil, can not create connection")
)

// Pool common connection pool
type Pool struct {
	New       func() (interface{}, error) // New create connection function
	Ping      func(interface{}) bool      // Ping check connection is ok
	Close     func(interface{})           // Close close connection
	store     chan interface{}
	mu        sync.Mutex
	closeOnce sync.Once
}

// NewClientConnPool New create a pool with capacity
func NewClientConnPool(initCap, maxCap int, newFunc func() (interface{}, error)) (*Pool, error) {
	if maxCap == 0 || initCap > maxCap {
		return nil, ConnectionPoolInvalidCapacityError
	}
	p := new(Pool)
	p.store = make(chan interface{}, maxCap)
	if newFunc != nil {
		p.New = newFunc
	}
	for i := 0; i < initCap; i++ {
		v, err := p.create()
		if err != nil {
			continue
		}
		p.store <- v
	}
	return p, nil
}

// Len returns current connections in pool
func (p *Pool) Len() int {
	return len(p.store)
}

// Get returns a conn form store or create one
func (p *Pool) Get() (interface{}, error) {
	if p.store == nil {
		// pool aleardy destroyed, returns error
		return nil, ConnectionPoolClosedError
	}
	for {
		select {
		case v := <-p.store:
			if p.Ping != nil && !p.Ping(v) { // ping 不通过，这个对象就抛弃掉，不会保存回 store
				continue
			}
			return v, nil
		default:
			// pool is empty, returns new connection
			return p.create()
		}
	}
}

// Put set back conn into store again
func (p *Pool) Put(v interface{}) {
	select {
	case p.store <- v:
		return
	default:
		// pool is full, close passed connection
		if p.Close != nil {
			p.Close(v)
		}
		return
	}
}

// Destroy clear all connections
func (p *Pool) Destroy() {
	p.closeOnce.Do(func() {
		p.mu.Lock()
		defer p.mu.Unlock()
		if p.store == nil {
			// pool aleardy destroyed
			return
		}
		close(p.store)
		for v := range p.store {
			if p.Close != nil {
				p.Close(v)
			}
		}
		p.store = nil
	})
}

func (p *Pool) create() (interface{}, error) {
	if p.New == nil {
		return nil, ConnectionPoolEmptyObjectError
	}
	return p.New()
}
