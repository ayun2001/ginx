package request

import (
	"context"
	"gitee.com/ayun2001/ginx/utils"
	"github.com/prometheus/client_golang/prometheus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"io"
	"strings"
	"time"
)

const promHostEndpoint = "host"

const (
	unary        = "unary"
	clientStream = "client_stream"
	serverStream = "server_stream"
	bidiStream   = "bidi_stream"
)

type grpcClientMetrics struct {
	name                   string
	clientNumber           prometheus.Gauge
	startedCount           *prometheus.CounterVec
	handledCount           *prometheus.CounterVec
	streamMsgReceived      *prometheus.CounterVec
	streamMsgSent          *prometheus.CounterVec
	handledDuration        *prometheus.HistogramVec
	streamSentDuration     *prometheus.HistogramVec
	streamReceivedDuration *prometheus.HistogramVec
}

func newGrpcClientMetrics(name string) *grpcClientMetrics {
	return &grpcClientMetrics{name: name}
}

func (m *grpcClientMetrics) registerMetrics() {
	var metricLabels = []string{"type", "service", "method", utils.ConstPromAppName, promHostEndpoint}
	var id = utils.Int64ToStr(utils.GetRandIdNumber())

	m.startedCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstPrometheusNamespace,
			Subsystem:   utils.ConstPrometheusSubsystemName,
			Name:        "grpc_client_started_total",
			Help:        "Total number of RPCs started on the client.",
			ConstLabels: map[string]string{"name": m.name, "id": id},
		}, metricLabels)

	m.handledCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstPrometheusNamespace,
			Subsystem:   utils.ConstPrometheusSubsystemName,
			Name:        "grpc_client_handled_total",
			Help:        "Total number of RPCs completed on the client, regardless of success or failure.",
			ConstLabels: map[string]string{"name": m.name, "id": id},
		}, append(metricLabels, "code"))

	m.streamMsgReceived = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstPrometheusNamespace,
			Subsystem:   utils.ConstPrometheusSubsystemName,
			Name:        "grpc_client_msg_received_total",
			Help:        "Total number of RPC stream messages received on the client.",
			ConstLabels: map[string]string{"name": m.name, "id": id},
		}, metricLabels)

	m.streamMsgSent = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstPrometheusNamespace,
			Subsystem:   utils.ConstPrometheusSubsystemName,
			Name:        "grpc_client_msg_sent_total",
			Help:        "Total number of gRPC stream messages sent by the server.",
			ConstLabels: map[string]string{"name": m.name, "id": id},
		}, metricLabels)

	m.handledDuration = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace:   utils.ConstPrometheusNamespace,
		Subsystem:   utils.ConstPrometheusSubsystemName,
		Name:        "grpc_client_handling_seconds",
		Help:        "Histogram of response latency (seconds) of gRPC that had been application-level handled by the server.",
		ConstLabels: map[string]string{"name": m.name, "id": id},
	}, metricLabels)

	m.streamSentDuration = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace:   utils.ConstPrometheusNamespace,
		Subsystem:   utils.ConstPrometheusSubsystemName,
		Name:        "grpc_client_msg_recv_handling_seconds",
		Help:        "Histogram of response latency (seconds) of the gRPC single message receive.",
		ConstLabels: map[string]string{"name": m.name, "id": id},
	}, metricLabels)

	m.streamReceivedDuration = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace:   utils.ConstPrometheusNamespace,
		Subsystem:   utils.ConstPrometheusSubsystemName,
		Name:        "grpc_client_msg_send_handling_seconds",
		Help:        "Histogram of response latency (seconds) of the gRPC single message send.",
		ConstLabels: map[string]string{"name": m.name, "id": id},
	}, metricLabels)

	m.clientNumber = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace:   utils.ConstPrometheusNamespace,
		Subsystem:   utils.ConstPrometheusSubsystemName,
		Name:        "grpc_client_pool_running_instance_number",
		Help:        "The total number of running instances in client pool",
		ConstLabels: map[string]string{utils.ConstPromAppName: utils.PromAppName, "name": m.name, "id": id},
	})

	prometheus.MustRegister(m.startedCount, m.handledCount, m.streamMsgReceived, m.streamMsgSent, m.handledDuration,
		m.streamSentDuration, m.streamReceivedDuration, m.clientNumber)
}

func (m *grpcClientMetrics) unRegisterMetrics() {
	prometheus.Unregister(m.startedCount)
	prometheus.Unregister(m.handledCount)
	prometheus.Unregister(m.streamMsgReceived)
	prometheus.Unregister(m.streamMsgSent)
	prometheus.Unregister(m.handledDuration)
	prometheus.Unregister(m.streamSentDuration)
	prometheus.Unregister(m.streamReceivedDuration)
	prometheus.Unregister(m.clientNumber)
}

func (m *grpcClientMetrics) Describe(ch chan<- *prometheus.Desc) {
	m.startedCount.Describe(ch)
	m.handledCount.Describe(ch)
	m.streamMsgReceived.Describe(ch)
	m.streamMsgSent.Describe(ch)
	m.handledDuration.Describe(ch)
	m.streamReceivedDuration.Describe(ch)
	m.streamSentDuration.Describe(ch)
}

func (m *grpcClientMetrics) Collect(ch chan<- prometheus.Metric) {
	m.startedCount.Collect(ch)
	m.handledCount.Collect(ch)
	m.streamMsgReceived.Collect(ch)
	m.streamMsgSent.Collect(ch)
	m.handledDuration.Collect(ch)
	m.streamReceivedDuration.Collect(ch)
	m.streamSentDuration.Collect(ch)

}

// unaryClientInterceptor is a gRPC client-side interceptor that provides Prometheus monitoring for Unary RPCs.
func (m *grpcClientMetrics) unaryClientInterceptor(extOpts *map[string]interface{}) func(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
	return func(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
		monitor := newClientReporter(m, unary, method, (*extOpts)[promHostEndpoint].(string))
		monitor.SentMessage()
		err := invoker(ctx, method, req, reply, cc, opts...)
		if err == nil {
			monitor.ReceivedMessage()
		}
		st, _ := status.FromError(err)
		monitor.Handled(st.Code())
		return err
	}
}

// streamClientInterceptor is a gRPC client-side interceptor that provides Prometheus monitoring for Streaming RPCs.
func (m *grpcClientMetrics) streamClientInterceptor(extOpts *map[string]interface{}) func(ctx context.Context, desc *grpc.StreamDesc, cc *grpc.ClientConn, method string, streamer grpc.Streamer, opts ...grpc.CallOption) (grpc.ClientStream, error) {
	return func(ctx context.Context, desc *grpc.StreamDesc, cc *grpc.ClientConn, method string, streamer grpc.Streamer, opts ...grpc.CallOption) (grpc.ClientStream, error) {
		monitor := newClientReporter(m, clientStreamType(desc), method, (*extOpts)[promHostEndpoint].(string))
		clientStream, err := streamer(ctx, desc, cc, method, opts...)
		if err != nil {
			st, _ := status.FromError(err)
			monitor.Handled(st.Code())
			return nil, err
		}
		return &monitoredClientStream{clientStream, monitor}, nil
	}
}

func clientStreamType(desc *grpc.StreamDesc) string {
	if desc.ClientStreams && !desc.ServerStreams {
		return clientStream
	} else if !desc.ClientStreams && desc.ServerStreams {
		return serverStream
	}
	return bidiStream
}

type monitoredClientStream struct {
	grpc.ClientStream
	monitor *clientReporter
}

func (s *monitoredClientStream) SendMsg(m interface{}) error {
	timer := s.monitor.SendMessageTimer()
	err := s.ClientStream.SendMsg(m)
	timer.ObserveDuration()
	if err == nil {
		s.monitor.SentMessage()
	}
	return err
}

func (s *monitoredClientStream) RecvMsg(m interface{}) error {
	timer := s.monitor.ReceiveMessageTimer()
	err := s.ClientStream.RecvMsg(m)
	timer.ObserveDuration()

	if err == nil {
		s.monitor.ReceivedMessage()
	} else if err == io.EOF {
		s.monitor.Handled(codes.OK)
	} else {
		st, _ := status.FromError(err)
		s.monitor.Handled(st.Code())
	}
	return err
}

type clientReporter struct {
	metrics     *grpcClientMetrics
	rpcType     string
	serviceName string
	methodName  string
	startTime   time.Time
	host        string
}

func newClientReporter(m *grpcClientMetrics, rpcType, fullMethod, host string) *clientReporter {
	r := &clientReporter{metrics: m, rpcType: rpcType, host: host}
	r.startTime = time.Now()
	r.serviceName, r.methodName = splitMethodName(fullMethod)
	r.metrics.startedCount.WithLabelValues(r.rpcType, r.serviceName, r.methodName, utils.PromAppName, r.host).Inc()
	return r
}

// timer is a helper interface to time functions.
type timer interface {
	ObserveDuration() time.Duration
}

func (r *clientReporter) ReceiveMessageTimer() timer {
	hist := r.metrics.streamReceivedDuration.WithLabelValues(r.rpcType, r.serviceName, r.methodName, utils.PromAppName, r.host)
	return prometheus.NewTimer(hist)
}

func (r *clientReporter) ReceivedMessage() {
	r.metrics.streamMsgReceived.WithLabelValues(r.rpcType, r.serviceName, r.methodName, utils.PromAppName, r.host).Inc()
}

func (r *clientReporter) SendMessageTimer() timer {

	hist := r.metrics.streamSentDuration.WithLabelValues(r.rpcType, r.serviceName, r.methodName, utils.PromAppName, r.host)
	return prometheus.NewTimer(hist)
}

func (r *clientReporter) SentMessage() {
	r.metrics.streamMsgSent.WithLabelValues(r.rpcType, r.serviceName, r.methodName, utils.PromAppName, r.host).Inc()
}

func (r *clientReporter) Handled(code codes.Code) {
	r.metrics.handledCount.WithLabelValues(r.rpcType, r.serviceName, r.methodName, utils.PromAppName, r.host, code.String()).Inc()
	r.metrics.handledDuration.WithLabelValues(r.rpcType, r.serviceName, r.methodName, utils.PromAppName, r.host).Observe(time.Since(r.startTime).Seconds())
}

func splitMethodName(fullMethodName string) (string, string) {
	fullMethodName = strings.TrimPrefix(fullMethodName, "/") // remove leading slash
	if i := strings.Index(fullMethodName, "/"); i >= 0 {
		return fullMethodName[:i], fullMethodName[i+1:]
	}
	return "unknown", "unknown"
}
