package redis

import (
	"context"
	"fmt"
	"gitee.com/ayun2001/ginx"
	"gitee.com/ayun2001/ginx/utils"
	"github.com/go-redis/redis/v8"
	"net"
	"sync"
	"time"
)

type Client struct {
	RedisDb    *redis.Client
	closeOnce  sync.Once
	stopCtx    context.Context
	stopCancel context.CancelFunc
	wg         sync.WaitGroup // go 关闭同步组
}

func init() {
	// 设置日志
	redis.SetLogger(newRedisZapLogger())
}

func NewRedisClient(config *ClientConf) *Client {
	// 判断地址
	if ip := net.ParseIP(config.Address); ip == nil {
		config.Address = "127.0.0.1"
	}
	// 判断端口
	if config.Port == 0 {
		config.Port = 6379
	}
	// 判断 DB 数字
	if config.DB > 15 {
		config.DB = 0
	}
	// 创建客户端
	r := new(Client)
	r.RedisDb = redis.NewClient(&redis.Options{
		Addr:         fmt.Sprintf("%s:%d", config.Address, config.Port),
		Password:     config.Password,
		DB:           int(config.DB),
		PoolSize:     config.MaxOpen,
		MinIdleConns: config.MinIdle,
		ReadTimeout:  time.Duration(config.ReadTimeout) * time.Millisecond,
		WriteTimeout: time.Duration(config.WriteTimeout) * time.Millisecond,
	})
	r.stopCtx, r.stopCancel = context.WithCancel(context.Background())
	// 连接测试下
	err := r.HealthCheck()
	if err != nil {
		ginx.Logs.Errorw("create redis init connection failed", "error", err)
		return nil
	}
	// 设置连接池监控守护
	go r.pingHealthCheckDaemon()
	r.wg.Add(1)
	// 正确创建了服务
	ginx.Logs.Infow(fmt.Sprintf("connect mysql %s:%d#%d success", config.Address, config.Port, config.DB))
	return r
}

// Close
// 这里优化了关闭顺序，优先健康监测关闭，要不然关闭出现监测失败
// 指针表示当前对象地址，没有指针表示数据副本是一个copy
func (c *Client) Close() {
	c.closeOnce.Do(func() {
		c.stopCancel()
		// 等待关闭守护
		c.wg.Wait()
		// 关闭连接池
		if c.RedisDb != nil {
			err := c.RedisDb.Close()
			if err != nil {
				ginx.Logs.Warnw("error when shutdown redis connection", "error", err)
			}
			ginx.Logs.Infow("redis client closed")
		}
	})
}

func (c *Client) HealthCheck() error {
	_, err := c.RedisDb.Ping(*utils.CreateCtxWithTimeout(ConstDbConnectTimeoutSec)).Result()
	return err
}

func (c *Client) pingHealthCheckDaemon() {
	ginx.Logs.Infow("redis health check daemon start")
	var ticker = time.NewTicker(time.Second * ConstHealthCheckStopTimeoutSec)

	defer func() {
		ticker.Stop()
		c.wg.Done()
		ginx.Logs.Infow("redis health check daemon shutdown")
	}()

	for {
		select {
		case <-ticker.C:
			// 执行探测
			if err := c.HealthCheck(); err != nil {
				ginx.Logs.Errorw("redis health check error", "error", err)
			}
			break
		case <-c.stopCtx.Done():
			return
		}
	}
}
