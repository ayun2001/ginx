package redis

const (
	ConstDbConnectTimeoutSec       = 10
	ConstHealthCheckStopTimeoutSec = 5
)

type ClientConf struct {
	Address      string `json:"address" yaml:"address"`
	Port         uint16 `json:"port" yaml:"port"`
	ReadTimeout  uint32 `json:"readTimeout,omitempty" yaml:"readTimeout,omitempty"`
	WriteTimeout uint32 `json:"writeTimeout,omitempty" yaml:"writeTimeout,omitempty"`
	Password     string `json:"password" yaml:"password"`
	DB           uint8  `json:"db" yaml:"db"`
	MinIdle      int    `json:"minIdle,omitempty" yaml:"minIdle,omitempty"`
	MaxOpen      int    `json:"maxOpen,omitempty" yaml:"maxOpen,omitempty"`
}
