package redis

import (
	"context"
	"gitee.com/ayun2001/ginx"
)

type redisLogger struct{}

func (r *redisLogger) Printf(ctx context.Context, msg string, data ...interface{}) {
	ginx.Logs.Infof(msg, data...)
}

func newRedisZapLogger() *redisLogger {
	return &redisLogger{}
}
