package log

import (
	"github.com/go-logr/logr"
	"github.com/go-logr/zapr"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
)

var (
	LoggerAtomicLevel = zap.NewAtomicLevelAt(zapcore.DebugLevel)
	validLogLevel     = map[string]zapcore.Level{
		"debug":  zap.DebugLevel,
		"info":   zap.InfoLevel,
		"warn":   zap.WarnLevel,
		"error":  zap.ErrorLevel,
		"dpanic": zap.DPanicLevel,
		"panic":  zap.PanicLevel,
		"fatal":  zap.FatalLevel,
	}
)

// LogLevel store zap atomic level
type LogLevel struct {
	value zapcore.Level
	name  string
}

// Set implement pflag.Value Set(string)
func (l *LogLevel) Set(flagValue string) error {
	name := flagValue
	level, exist := validLogLevel[flagValue]
	if !exist {
		level = zap.InfoLevel
		name = level.String()
	}
	l.name = name
	l.value = level
	return nil
}

// String implement pflag.Value String()
func (l *LogLevel) String() string {
	return l.name
}

// Type return pflag type
func (l *LogLevel) Type() string {
	return "level"
}

// Value return zap core level value
func (l *LogLevel) Value() zapcore.Level {
	return l.value
}

// 编码配置
func encoderConfig() zapcore.EncoderConfig {
	config := zapcore.EncoderConfig{
		TimeKey:        "time",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "caller",
		FunctionKey:    zapcore.OmitKey,
		MessageKey:     "message",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.CapitalLevelEncoder,
		EncodeTime:     zapcore.ISO8601TimeEncoder, // 自定义输出时间格式
		EncodeDuration: zapcore.StringDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}
	return config
}

// NewAndInitLogger create SugaredLogger with the filename, line number,
// and function name of zap's caller and set in developyment mode
func NewAndInitLogger(opts ...zap.Option) *zap.SugaredLogger {
	lg := NewZapLogger(opts...)
	zap.ReplaceGlobals(lg) // 替换zap包中全局的logger实例，后续在其他包中只需使用zap.L()调用即可
	return lg.Sugar()
}

func NewZapLogger(opts ...zap.Option) *zap.Logger {
	writer := zapcore.AddSync(os.Stdout) // 只使用终端输出
	encoderConfig := encoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder   // 修改时间戳的格式
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder // 日志级别使用大写显示
	encoder := zapcore.NewJSONEncoder(encoderConfig)
	core := zapcore.NewCore(encoder, writer, zapcore.DebugLevel) // 将日志级别设置为 DEBUG
	return zap.New(core, zap.AddCaller(), zap.Development()).WithOptions(opts...)
}

func NewLogrStandardLogger(opts ...zap.Option) logr.Logger {
	return zapr.NewLogger(NewZapLogger(opts...))
}

func ZapLoggerToSugaredLogger(l *zap.Logger) *zap.SugaredLogger {
	zap.ReplaceGlobals(l)
	return l.Sugar()
}
