package signal

import (
	"context"
	"sync"
)

type TermSignal struct {
	ctx       context.Context
	cancel    context.CancelFunc
	wg        sync.WaitGroup
	callbacks []func()
	stopCh    chan struct{}
}

func NewTermSignal() *TermSignal {
	var t TermSignal
	t.ctx, t.cancel = context.WithCancel(context.Background())
	t.stopCh = make(chan struct{})
	return &t
}

func (s *TermSignal) CancelCallbacksRegistry(callbacks ...func()) {
	s.callbacks = append(s.callbacks, callbacks...)
}

// GetStopChannel 不用手动关闭这个 channel， 关闭函数最后会关闭 channel
func (s *TermSignal) GetStopChannel() *chan struct{} {
	return &s.stopCh
}

func (s *TermSignal) GetStopCtx() *context.Context {
	return &s.ctx
}

func (s *TermSignal) Close(wg *sync.WaitGroup) {
	// 启动通知停止工作
	for _, c := range s.callbacks {
		if c != nil {
			s.wg.Add(1)
			go func(cc func()) {
				select {
				case <-s.ctx.Done(): // 监听关闭信号
					cc()
					break
				}
				s.wg.Done()
			}(c) // 这里一定传参，有临界的问题
		}
	}
	// 退出动作
	if s.stopCh != nil { // channel 关闭
		close(s.stopCh)
	}
	s.cancel()
	// 等待结束
	s.wg.Wait()
	// 关闭外部信号
	if wg != nil {
		wg.Done()
	}
}
