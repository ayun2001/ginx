package signal

import (
	"os"
	"os/signal"
	"sync"
	"syscall"
)

func WaitTermSignalForGracefulRestart(sigs ...*TermSignal) {
	// 捕获系统信号量
	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	close(quit)
	// 相应多个关闭信号
	if sigs != nil && len(sigs) > 0 {
		var wg sync.WaitGroup
		wg.Add(len(sigs))
		for _, s := range sigs {
			go s.Close(&wg)
		}
		wg.Wait()
	}
}
