package utils

import (
	"errors"
	"strings"
)

const (
	ConstPromAppName                   = "app"
	ConstPrometheusNamespace           = "ginx"
	ConstPrometheusSubsystemName       = ""
	ConstHttpRequestIdleConnTimeout    = 15 * 1000
	ConstGrpcClientPingMinIntervalTime = 5 * 1000
)

var (
	PromAppName           = "default"
	InvalidIpAddressError = errors.New("invalid IP address")
)

func SetPromMetricsAppName(name string) {
	PromAppName = strings.TrimSpace(name)
}
