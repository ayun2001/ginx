package utils

import (
	"context"
	"fmt"
	"github.com/cespare/xxhash/v2"
	"regexp"
	"strconv"
	"strings"
	"time"
	"unsafe"
)

var (
	// 错误就直接抛出异常
	regDupWS        = regexp.MustCompile(`\s+`)
	regDupUrlSlash  = regexp.MustCompile(`//+`)
	regHttpUrlSplit = regexp.MustCompile(`(?P<schema>https?:/)(?P<url>/.+)`)
)

const (
	defaultContextTimeoutSec = 3
)

// RemoveDuplicateWhitespace 删除字符串中重复的空白字符，只保留 为单个空白字符
// trim: 是否去掉首位空白
func RemoveDuplicateWhitespace(s string, trim bool) string {
	s = regDupWS.ReplaceAllString(s, " ")
	if trim {
		s = strings.TrimSpace(s)
	}
	return s
}

// RemoveDuplicateUrlSlash 去除 url 中重复的 /
func RemoveDuplicateUrlSlash(s string) string {
	items := regHttpUrlSplit.FindStringSubmatch(s)
	if len(items) <= 2 {
		return s
	}
	return items[1] + regDupUrlSlash.ReplaceAllString(items[2], "/")
}

// TruncateDecimal4 uint 代表小数位数，格式位 0.000001 如果是几位就指定为几位
func TruncateDecimal4(value float64) float64 {
	value, _ = strconv.ParseFloat(fmt.Sprintf("%.4f", value), 64)
	return value
}

// CreateCtxWithTimeout 创建 context
func CreateCtxWithTimeout(sec uint32) *context.Context {
	var ctx context.Context
	if sec > 0 {
		ctx, _ = context.WithTimeout(context.Background(), time.Duration(sec)*time.Second)
	} else {
		ctx, _ = context.WithTimeout(context.Background(), defaultContextTimeoutSec*time.Second)
	}
	return &ctx
}

// BytesToStr 字节转字符串
func BytesToStr(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}

// StrToBytes 字符串转字节
func StrToBytes(s string) []byte {
	x := (*[2]uintptr)(unsafe.Pointer(&s))
	h := [3]uintptr{x[0], x[1], x[1]}
	return *(*[]byte)(unsafe.Pointer(&h))
}

func Uint64ToStr(n uint64) string {
	return strconv.FormatUint(n, 10)
}

func Int64ToStr(n int64) string {
	return strconv.FormatInt(n, 10)
}

func IntToStr(n int) string {
	return strconv.Itoa(n)
}

func XXHashStringToUint64(s string) uint64 {
	return xxhash.Sum64String(s)
}

func XXHashBytesToUint64(b []byte) uint64 {
	return xxhash.Sum64(b)
}
