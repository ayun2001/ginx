package utils

import (
	"github.com/bwmarrin/snowflake"
	"math/rand"
	"time"
)

var snowflakeNode, _ = snowflake.NewNode(GetRand().Int63n(-1 ^ (-1 << snowflake.NodeBits)))

func GetCurrentMilliSecTimestamp() int64 {
	return time.Now().UTC().UnixMicro()
}

func GetRand() *rand.Rand {
	return rand.New(rand.NewSource(time.Now().UnixNano()))
}

func GetRandIdNumber() int64 {
	return snowflakeNode.Generate().Int64()
}
