package request

import "gitee.com/ayun2001/ginx"

type requestLogger struct{}

func newRequestLogger() *requestLogger {
	return &requestLogger{}
}

func (r *requestLogger) Errorf(format string, v ...interface{}) {
	ginx.Logs.Errorf(format, v)
}

func (r *requestLogger) Warnf(format string, v ...interface{}) {
	ginx.Logs.Warnf(format, v)
}

func (r *requestLogger) Debugf(format string, v ...interface{}) {
	ginx.Logs.Debugf(format, v)
}
