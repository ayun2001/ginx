package request

import (
	"bytes"
	"errors"
	"gitee.com/ayun2001/ginx/utils"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"strings"
)

var DetermineRequestContentTypeError = errors.New("failed to determine the request content type. set http header with Content-Type")

func GenerateRequestPath(c *gin.Context) string {
	path := c.Request.URL.Path
	if len(c.Request.URL.RawQuery) > 0 {
		path = strings.Join([]string{c.Request.URL.Path, c.Request.URL.RawQuery}, "?")
	}
	return path
}

func GenerateRequestBody(c *gin.Context) string {
	body, err := c.GetRawData()
	if err != nil {
		body = utils.StrToBytes("get request body failed ")
	}
	c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(body))
	return utils.BytesToStr(body)
}

type ParseBodyContentOpts struct {
	FlexibleMatching      bool // 可以支持局部解析 (解析对象是传入参数的子集)
	AllowEmptyBodyContent bool
}

type ParseOptsCallback func(*ParseBodyContentOpts)

func ParseReqBodyWithFlexibleMatching(b bool) ParseOptsCallback {
	return func(o *ParseBodyContentOpts) {
		o.FlexibleMatching = b
	}
}

func ParseReqBodyWithAllowEmptyContent(b bool) ParseOptsCallback {
	return func(o *ParseBodyContentOpts) {
		o.AllowEmptyBodyContent = b
	}
}

func ParseRequestBody(c *gin.Context, b interface{}, opts ...ParseOptsCallback) error {
	if c.ContentType() == "" {
		return DetermineRequestContentTypeError
	}

	// 默认参数
	options := ParseBodyContentOpts{}

	// 覆盖参数
	for _, opt := range opts {
		opt(&options)
	}

	var body string
	if options.FlexibleMatching || options.AllowEmptyBodyContent {
		body = strings.TrimSpace(GenerateRequestBody(c))
	}

	// 判断传入参数
	if err := c.ShouldBind(b); err != nil {
		if options.AllowEmptyBodyContent && len(body) == 0 {
			return nil
		}
		return err
	}

	// 修复 request body， 防止局部解析丢失之前的 body 内容
	if options.FlexibleMatching {
		c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(utils.StrToBytes(body)))
	}

	// 返回结果
	return nil
}
