package request

import (
	"gitee.com/ayun2001/ginx/utils"
	"github.com/prometheus/client_golang/prometheus"
	"net/http"
)

type httpClientMetrics struct {
	name              string
	clientNumber      prometheus.Gauge
	requestCount      *prometheus.CounterVec
	requestDuration   *prometheus.HistogramVec
	requestSizeBytes  *prometheus.SummaryVec
	responseSizeBytes *prometheus.SummaryVec
}

func newHttpClientMetrics(name string) *httpClientMetrics {
	return &httpClientMetrics{name: name}
}

func (m *httpClientMetrics) registerMetrics() {
	var metricLabels = []string{"status", "method", "host", "path", utils.ConstPromAppName}
	var id = utils.Int64ToStr(utils.GetRandIdNumber())

	m.requestCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   utils.ConstPrometheusNamespace,
			Subsystem:   utils.ConstPrometheusSubsystemName,
			Name:        "http_client_request_count",
			Help:        "Total number of HTTP requests made.",
			ConstLabels: map[string]string{"name": m.name, "id": id},
		}, metricLabels,
	)

	m.requestDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace:   utils.ConstPrometheusNamespace,
			Subsystem:   utils.ConstPrometheusSubsystemName,
			Name:        "http_client_request_duration_seconds",
			Help:        "HTTP request latencies in seconds.",
			ConstLabels: map[string]string{"name": m.name, "id": id},
		}, metricLabels,
	)

	m.requestSizeBytes = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Namespace:   utils.ConstPrometheusNamespace,
			Subsystem:   utils.ConstPrometheusSubsystemName,
			Name:        "http_client_request_size_bytes",
			Help:        "HTTP request sizes in bytes.",
			ConstLabels: map[string]string{"name": m.name, "id": id},
		}, metricLabels,
	)

	m.responseSizeBytes = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Namespace:   utils.ConstPrometheusNamespace,
			Subsystem:   utils.ConstPrometheusSubsystemName,
			Name:        "http_client_response_size_bytes",
			Help:        "HTTP response sizes in bytes.",
			ConstLabels: map[string]string{"name": m.name, "id": id},
		}, metricLabels,
	)

	m.clientNumber = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace:   utils.ConstPrometheusNamespace,
		Subsystem:   utils.ConstPrometheusSubsystemName,
		Name:        "http_client_pool_running_instance_number",
		Help:        "The total number of running instances in client pool",
		ConstLabels: map[string]string{utils.ConstPromAppName: utils.PromAppName, "name": m.name, "id": id},
	})

	prometheus.MustRegister(m.requestCount, m.requestDuration, m.requestSizeBytes, m.responseSizeBytes, m.clientNumber)
}

func (m *httpClientMetrics) unRegisterMetrics() {
	prometheus.Unregister(m.requestCount)
	prometheus.Unregister(m.requestDuration)
	prometheus.Unregister(m.requestSizeBytes)
	prometheus.Unregister(m.responseSizeBytes)
	prometheus.Unregister(m.clientNumber)
}

// calcRequestSize returns the size of request object.
func calcRequestSize(r *http.Request) float64 {
	size := 0
	if r.URL != nil {
		size = len(r.URL.String())
	}

	size += len(r.Method)
	size += len(r.Proto)

	for name, values := range r.Header {
		size += len(name)
		for _, value := range values {
			size += len(value)
		}
	}
	size += len(r.Host)

	// r.Form and r.MultipartForm are assumed to be included in r.URL.
	if r.ContentLength != -1 {
		size += int(r.ContentLength)
	}
	return float64(size)
}
