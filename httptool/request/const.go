package request

const (
	ConstHttpHeaderContentTypeKey       = "Content-Type"
	ConstHttpHeaderJsonContentTypeValue = "application/json; charset=utf-8"
	ConstHttpHeaderXmlContentTypeValue  = "application/xml; charset=utf-8"
)

const XRequestIDHeaderKey = "X-Request-Id"
