package request

import (
	"crypto/tls"
	"errors"
	"gitee.com/ayun2001/ginx"
	"gitee.com/ayun2001/ginx/codec/json"
	"gitee.com/ayun2001/ginx/utils"
	"github.com/go-resty/resty/v2"
	"net/http"
	"reflect"
	"sync"
	"time"
)

const defaultClientGCTimeout = 5 * 60 * 1000 // 5 分钟

var ResponsePtrIsNullError = errors.New("response pointer is nil")

type HttpClientPoolConf struct {
	DisableCompression bool   `json:"disableCompression,omitempty" yaml:"disableCompression,omitempty"` // 关闭 gzip 解压
	DisableKeepalive   bool   `json:"disableKeepalive,omitempty" yaml:"disableKeepalive,omitempty"`     // 关闭 keepalive 长连接
	ConnTimeout        uint32 `json:"connectTimeout,omitempty" yaml:"connectTimeout,omitempty"`         // client 连接服务器超时时间
	IdleTimeout        uint32 `json:"idleTimeout,omitempty" yaml:"idleTimeout,omitempty"`               // client 会话空闲超时时间
	ClientGCTimeout    uint32 `json:"clientGCTimeout,omitempty" yaml:"clientGCTimeout,omitempty"`       // client 尝试没有被使用，被回收的时间
}

type HttpClientPool struct {
	clientMetrics *httpClientMetrics
	conf          *HttpClientPoolConf
	closeOnce     sync.Once
	instance      *resty.Client
	mux           sync.RWMutex
}

type nameStoreItem struct {
	updateTime time.Time
	session    resty.Client
}

func NewHttpClientPool(name string, poolConf *HttpClientPoolConf) *HttpClientPool {
	if len(name) <= 0 {
		name = "clientpool_" + utils.Int64ToStr(utils.GetRandIdNumber())
	}

	poolConf = validHttpClientConfig(poolConf)

	var p = HttpClientPool{conf: poolConf}
	p.clientMetrics = newHttpClientMetrics(name)
	p.clientMetrics.registerMetrics()

	p.mux.Lock()
	p.instance = p.NewClient()
	p.mux.Unlock()

	return &p
}

// NewClient 创建 resty 对象，连接不复用
func (p *HttpClientPool) NewClient() *resty.Client {
	r := resty.New()
	r.SetLogger(newRequestLogger())
	r.JSONMarshal = json.Marshal
	r.JSONUnmarshal = json.Unmarshal
	r.SetTimeout(time.Millisecond * time.Duration(p.conf.ConnTimeout))
	r.SetTransport(&http.Transport{
		DisableCompression: p.conf.DisableCompression,
		IdleConnTimeout:    time.Millisecond * time.Duration(p.conf.IdleTimeout),
		DisableKeepAlives:  p.conf.DisableKeepalive,
		TLSClientConfig:    &tls.Config{InsecureSkipVerify: true},
	})
	r.SetRedirectPolicy(resty.FlexibleRedirectPolicy(15))
	r.SetAllowGetMethodPayload(true) // 允许 get 方法带 body
	r.AddRetryCondition(
		// RetryConditionFunc type is for retry condition function
		// input: non-nil Response OR request execution error
		func(r *resty.Response, err error) bool {
			return r.StatusCode() == http.StatusTooManyRequests
		},
	)
	//r.OnBeforeRequest(func(c *resty.Client, req *resty.Request) error {
	//	return nil // if its success otherwise return error
	//})
	r.OnAfterResponse(func(c *resty.Client, resp *resty.Response) error {
		if resp == nil {
			return ResponsePtrIsNullError
		}
		var respBody string
		if resp.Body() != nil {
			respBody = utils.BytesToStr(resp.Body())
		}
		if resp.Request.Body != nil &&
			(reflect.TypeOf(resp.Request.Body).Kind() == reflect.Array || reflect.TypeOf(resp.Request.Body).Kind() == reflect.Slice) &&
			reflect.TypeOf(resp.Request.Body).Elem().Kind() == reflect.Uint8 {
			resp.Request.Body = utils.BytesToStr(resp.Request.Body.([]byte))
		}
		// 变量
		host := resp.Request.RawRequest.URL.Host
		path := resp.Request.RawRequest.URL.Path
		status := utils.IntToStr(resp.StatusCode())
		// 记录 metrics
		p.clientMetrics.requestCount.WithLabelValues(status, resp.Request.Method, host, path, utils.PromAppName).Inc()
		p.clientMetrics.requestDuration.WithLabelValues(status, resp.Request.Method, host, path, utils.PromAppName).Observe(float64(resp.Time().Milliseconds()))
		p.clientMetrics.requestSizeBytes.WithLabelValues(status, resp.Request.Method, host, path, utils.PromAppName).Observe(calcRequestSize(resp.Request.RawRequest))
		p.clientMetrics.responseSizeBytes.WithLabelValues(status, resp.Request.Method, host, path, utils.PromAppName).Observe(float64(resp.Size()))
		// 记录日志
		ginx.Logs.Infow("resty http client",
			"host", host,
			"path", path,
			"method", resp.Request.Method,
			"code", resp.StatusCode(),
			"success", resp.IsSuccess(),
			"queryParams", resp.Request.QueryParam.Encode(),
			"requestBody", resp.Request.Body,
			"responseBody", respBody,
		)
		// 不用主动处理 http client 关闭，防止请求内存泄露, 除非调用 SetDoNotParseResponse(true) 要处理 http.client body 的close
		return nil
	})

	return r
}

func (p *HttpClientPool) SetClient(c *resty.Client) {
	if c != nil {
		p.mux.Lock()
		p.instance = c
		p.mux.Unlock()
	}
}

// GetRequest 复用 client， 支持 http keepalive，并更新获得 client 时间
func (p *HttpClientPool) GetRequest() *resty.Request {
	p.mux.RLock()
	defer func() {
		p.mux.RUnlock()
		p.clientMetrics.clientNumber.Inc()
	}()
	return p.instance.NewRequest()
}

// PutRequest 归还会话
func (p *HttpClientPool) PutRequest(_ interface{}) {
	p.clientMetrics.clientNumber.Dec()
}

func (p *HttpClientPool) Close() {
	p.closeOnce.Do(func() {
		p.clientMetrics.unRegisterMetrics()
	})
}

func validHttpClientConfig(conf *HttpClientPoolConf) *HttpClientPoolConf {
	if conf != nil {
		if conf.ConnTimeout < utils.ConstHttpRequestIdleConnTimeout {
			conf.ConnTimeout = utils.ConstHttpRequestIdleConnTimeout
		}
		if conf.IdleTimeout < utils.ConstHttpRequestIdleConnTimeout {
			conf.IdleTimeout = utils.ConstHttpRequestIdleConnTimeout
		}
		if conf.ClientGCTimeout < defaultClientGCTimeout {
			conf.ClientGCTimeout = defaultClientGCTimeout
		}
		return conf
	} else {
		return &HttpClientPoolConf{
			ConnTimeout:     utils.ConstHttpRequestIdleConnTimeout,
			IdleTimeout:     utils.ConstHttpRequestIdleConnTimeout,
			ClientGCTimeout: defaultClientGCTimeout,
		}
	}
}
